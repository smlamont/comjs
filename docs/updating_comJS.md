Notes on using and developing comjs:
=====================================

Adding new code and features
----------------------------
**Rule #1:**
Code is never committed to the master branch.

**Rule #2:**
If you want to add a bug fix or feature, do the following:

1. Checkout the develop branch and pull the latest:
    
`git checkout develop && git pull`

2. Create a new feature or bug fix branch:

`git checkout -b feature/my_feature_name`

OR

`git checkout -b bugfix/my_fix_name`

3. Push the branch to the remote:

`git push --set-upstream origin feature/my_feature_name`

4. Add and commit your code changes (this could be one or more commits).

5. When your branch is finished and tested, create a pull request on the git server to get it merged into develop:

`https://github.com/<xxx>/comJS/compare/develop...feature/my_feature_name?expand=1`

Be sure to add a good description in your pull request.

6. Ask an administrator to review and merge your pull request.

**Rule #3**
When the develop branch has had one or more pull requests merged into it, the administrator may decide to create a new release.
The decision of when to create a release has no hard and fast rule.
The release procedure is outlined here:

Doing a new release
-------------------
1. Choose a new version number. Follow [semantic versioning](http://semver.org/) rules.

2. Checkout the develop branch and pull the latest:

 `git checkout develop && git pull`


3. Update the version number in package.json

5. Update the version number image at the top of readme.md

6. Commit and push to develop:

`git commit -am "Update to version x.x.x" && git push`

8. Checkout master and pull latest:

`git checkout master && git pull`

9. Merge develop into master:

`git merge develop`

10. Tag the release:

`git tag -a -m "version x.x.x" x.x.x`

11. Push the merge and tag to origin:

`git push && git push --tags`

11. Go to the [github release page](https://github.com/<xxx>/comJS/releases) and add release notes.
