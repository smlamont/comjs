City of Toronto Proxy Workarounds
=================================

You will need to do proxy settings when on the COT network. You can use these commands in your terminal.

npm
---
Run the following commands to set up all npm projects to use a proxy. These commands should only have to be done once on your workstation.

`npm config set proxy "http://proxy.toronto.ca:8080" --global`

`npm config set https-proxy "http://proxy.toronto.ca:8080" --global`

git
---
Run the following commands to set up all git projects to use a proxy. Note that you MUST be in a git directory when you run this command. The simplest way to accomplish this is to create a temporary git directory first:  

1. Create a temporary directory: `mkdir temp`
2. Go into the new directory: `cd temp`
3. Initialize it as a git directory: `git init`
4. Configure your proxy setting, globally: `git config http.proxy http://proxy.toronto.ca:8080 --global`
5. Get out of the directory: `cd ..`
6. Delete the temp directory. On a mac: `rm -r temp`. On Windows: `rmdir temp /s`

environment variables
---------------------
Set up ENV variables within your command line. These are used by various tools, including bower.

**On a Mac:**

`export HTTP_PROXY=http://proxy.toronto.ca:8080`

`export http_proxy=http://proxy.toronto.ca:8080`

`export HTTPS_PROXY=http://proxy.toronto.ca:8080`

`export https_proxy=http://proxy.toronto.ca:8080`

**On a PC:**

1. Go to Control Panel > System > Advanced System Settings
2. Click on the Environment Variables... button
3. Add http_proxy and https_proxy as variables, both with the same value: http://proxy.toronto.ca:8080

Alternately, you can set these via command line, but you will have to execute them every time you open your command prompt:
 
`set http_proxy=http://proxy.toronto.ca:8080`

`set https_proxy=http://proxy.toronto.ca:8080`

> **Important!**
> A note about these proxy settings: sometimes the proxy stops working or doesn't work because your username and password isn't in the  proxy URL.
> You can 'refresh your proxy session' by keeping a web browser open on your system, and navigating to an external website (something not in your browser cache is probably best) when you run into issues. This seems to 'reset' how the proxy server allows your machine on to the proxy without a username/password in the URL.
