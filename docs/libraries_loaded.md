 |Libraries |Standalone Apps |Embedded Apps |
 |----------|----------------|--------------|
 |[ jquery 2.2.4 ](http://jquery.com/) | ​Included via injection | Included via website |
 |[ ​bootstrap 3.3.7 ](https://getbootstrap.com/docs/3.3/css/) | ​Included via injection | Included via website |
 | html5shiv, respond.js  | ​Included via injection | Included via website |
 | roboto fonts  | ​Included via injection | Included via website |
 | comJS_app, bootstrap_comJS_customizations  | ​Included via injection | Not available |
 | embedded_comjS_app  | ​Included via injection | Not available |
 |[ jquery.cookie.js ](https://github.com/carhartl/jquery-cookie) | ​Included via injection | |
 |[ backbone ](http://backbonejs.org) [ underscore ](http://underscorejs.org/), comJS_backbone| Optional injection via comJSConfig.​includeModeling​ | |
 |[ ​formValidation ](http://formvalidation.io/), comJS_forms | ​Optional injection via comJSConfig.includeFormValidation | |
 |[ jintlTelInput ](https://github.com/jackocnr/intl-tel-input) | ​Optional injection via comJSConfig.includeIntlTelInput | |
 |[ jquery.maskedinput ](http://digitalbush.com/projects/masked-input-plugin/) | ​Optional injection via comJSConfig.includeJQueryMaskedInput | |
 |[ jquery-editable-select ](http://indrimuska.github.io/jquery-editable-select/) | ​Optional injection via comJSConfig.includeEditableSelect | |
 | ​placeholders.jquery | Optional injection via comJSConfig.includePlaceholders| |
 |[ bootstrap-multiselect ](http://davidstutz.github.io/bootstrap-multiselect/) | ​Optional injection via comJSConfig.includeMultiSelect | |
 |[ OMS for google maps ](https://github.com/jawj/OverlappingMarkerSpiderfier) | Optional injection via comJSConfig.includeOMS | |
 |[ moment.js ](http://momentjs.com/) |  Included via injection if any of the following are true:| |
 || 		comJSConfig.includeMoment| |
 ||         comJSConfig.includeFullCalendar| |
 ||         comJSConfig.includeDatePicker| |
 ||         comJSConfig.includeRangePicker| |
 | fullcalendar | Optional injection via comJSConfig.includeFullCalendar | |
 |[ bootstrap-datetimepicker ](http://eonasdan.github.io/bootstrap-datetimepicker/) | Optional injection via comJSConfig.includeDatePicker | |
 |[ daterangepicker ](http://www.daterangepicker.com/) | Optional injection via comJSConfig.includeRangePicker | |
 |[ bootbox](http://bootboxjs.com/) | Optional injection via comJSConfig.includeBootbox | |
 |[ dropzone ](http://www.dropzonejs.com/) | Optional injection via comJSConfig.includeDropzone | |
 | comJS_modal | Optional injection via comJSConfig.includeModal | |
 | comJS_terms | Optional injection via comJSConfig.includeTerms | |
 |​​embedded_comJS_forms (CSS) | Optional injection via comJSConfig.includeFormValidation | |
 
