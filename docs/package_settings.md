Configuring your package.json file
==================================
The options in your package.json file determine how comJS builds your application.
Configuring this file should be one of the first things you do on a new project.

Basic properties
----------------
**name**:
This is the name of your application, and is set automatically when you scaffold your project.
You should not have to change this property.

**version**:
All new projects start at version 0.0.0.
You should not have to update this property. The `gulp release` comJS task will update this for you.

**qaRelease**:
This indicates the current QA release of your project.
You should not have to update this property. The `gulp release` comJS task will update this for you.

**description**:
This is a short description of your project.
This is useful for other developers that may work on your project in the future.

**author**:
This should be set to your name.
This is useful for other developers that may work on your project in the future.

**contributors**:
If there are many people working on this project, add them here.
This is useful for other developers that may work on your project in the future.

devDependencies property
------------------------
These are the npm packages that your application requires, and are set during scaffolding.
You should not need to update this property.

comJSConfig properties
---------------------
These options determine how comJS builds your project, and which libraries are included.
To minimize bloat, you should only set these to true if you need those features.
Check out this [table](libraries_loaded.md) for specific details on what JS/CSS files are loaded for your project based on comJSConfig properties.

**isEmbedded**:
When set to true, your project is built as a WordPress-embedded app that will be deployed to the city's word press site.:
When set to false, your project is built as a standalone app that can be deployed to internal web servers.

**includeIntlTelInput**:
If you want to use a telephone formatted input field (you use any validationtype=Phone comJSform fields), set this to true.
Your app will use the [intl-tel-input](https://github.com/jackocnr/intl-tel-input) plugin.

**includeJQueryMaskedInput**:
If you want to use masked input fields for auto-formatting, set this to true.
Your app will use the [jquery.maskedinput](http://digitalbush.com/projects/masked-input-plugin/) plugin.

**includeFormValidation**:
If you want to build a form, you should set this to true.
Your application will use the comJS_form.js library, along with form validation and other plugins.

**includeEditableSelect**:
If you need to use a fancy dropdown field with features that go beyond a standard dropdown, you should set this to true.
Your application will have the jquery-editable-select plugin added.

**includePlaceholders**:
If you need to support older browsers, and you want to use placeholders on forms, you should set this to true.
Your application will add support for the html placeholder attribute in older browsers.

**includeMultiSelect**:
If you need to use a `<select multiple>` field and you want to customize it to look and act nicely, you should set this to true.  
Your application will have the bootstrap-multiselect plugin added, along with the comJS_multiselect.js helper file.

**includeOMS**:
If you are using google maps and you have lots of markers that are clustered together, you should set this to true.
Your application will have the google maps OMS plugin added.

**includeFullCalendar**:
If you need a nice calendar widget, you should set this to true.
Your application will have the fullcalendar plugin added.

**includeDatePicker**:
If you need a date picker field or you use type=datetimepicker on your comJS_form, you should set this to true.
Your application will have the bootstrap-datetimepicker plugin added.

**includeRangePicker**:
If you need a date range picker field or you use type=daterangepicker on your comJS_form, you should set this to true.
Your application will have the bootstrap-daterangepicker plugin added.

**includeMoment**:
If you need to manipulate or format dates, you should set this to true.
Your application will have the moment.js library added.

**includeModeling**:
If you'd like a set of javascript tools for modeling, routing, events, and more, then set this to true.
Your application will have the Backbone and Underscore javascript libraries added.

**includeBootbox**:
If you'd like a fancy and simple way to do javascript alerts and prompts, then set this to true.
Your application will have the bootbox.js library added.

**includeDropzone**:
If you'd like a fancy way to do file uploads, then set this to true.
Your application will have the dropzone file uploader added.

**includeModal**:
If you need to display modal bootstrap dialogs, then set this to true.
Your application will have the comJS_modal.js helper file added.

**includeTerms**:
If you need to display a Terms and Conditions dialog, then set this to true.
Your application will have the comJS_terms.js helper file added.

