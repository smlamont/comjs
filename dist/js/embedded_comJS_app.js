var ComJSApp = function () {
  this.appContentKeySuffix = '/';
};

ComJSApp.prototype.loadAppContent = function (o) {
  var data = {},
    options = $.extend({
      keys: [], //an array of titles of API Content Snippets from WP
      tag: '', //a future option, where you can get multiple snippets with a single tag
      onComplete: function (data) { //called after all API calls are completed
        //data - a key/value hash with each key name and the associated fetched data value
      },
      onProgress: function (keyName, errXhr, errMsgOne, errMsgTwo) { //called after a single key is loaded successfully or not successfully
        //keyName - the key that was just finished
        //errXhr, errMsgOne, errMsgTwo - error information if the API call failed
      }
    }, o),
    this1 = this;

  if (options.tag) {
    //TODO: grab all content for a given tag
  } else {
    var count = 0;
    options.keys.forEach(function (key) {
      $.ajax({
        url: '/app_content/' + key + this1.appContentKeySuffix,
        success: function (result) {
          data[key] = result;
          options.onProgress(key);
        },
        error: function (a, b, c) {
          options.onProgress(key, a, b, c);
        },
        complete: function () {
          count++;
          if (count >= options.keys.length) {
            options.onComplete(data);
          }
        }
      });
    });
    if (options.keys.length === 0) {
      options.onComplete(data);
    }
  }

};
