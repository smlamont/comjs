/*
Showing a terms and conditions agreement is a common pattern. This helper method encapsulates a common approach

If includeTerms is set to true in your project's package.json coreConfig, then this file will be included.
The method below is a static method of the comJS_app (if your app is standalone) or ComJSApp (if your app is embedded) class.
Use it like this:

ComJSApp.showTerms({termsText: 'Agree to these terms', disagreedText: 'Why not?', containerSelector: '#my_element'}); //embedded apps

comJS_app.showTerms({termsText: 'Agree to these terms', disagreedText: 'Why not?', containerSelector: '#my_element'}); //standalone apps

 */
$(function(){
  var appClass = (window['comJS_app'] || window['ComJSApp']);
  appClass['showTerms'] = function (o) {
    var options = $.extend({
      termsText: 'Content missing: terms text', //The body text of the terms and conditions
      disagreedText: 'Content missing: terms disagreed text', //The body text to show when someone disagrees with the terms and conditions
      agreedCookieName: 'terms_cookie' + Math.random().toString().split('.')[1], //The name of the cookie to store the user's agreement in
      containerSelector: '', //A CSS selector for an element on screen where the terms and conditions will be shown
      onAgreed: function (termsWereShown) { //A function called after the user clicks the agree button
        //termsWereShown - true if the terms were shown before agreement. false if the user had previously agreed and the cookie was still there, bypassing the terms
      },
      onDisagreed: function () { //A function called after the user clicks the disagree button
      },
      agreementTitle: 'Terms of Use Agreement' //The title to show at the top
    }, o);

    if (!options.containerSelector) {
      throw new Error('missing container selector for showTerms');
    }

    if ($.cookie(options.agreedCookieName) !== "agree") {
      $(options.containerSelector).html('<section id="comJS-terms">' +
        '<div id="comJS-terms-title"><h2 tabindex="-1">' + options.agreementTitle + '</h2></div>' +
        '<div class="row">' +
        '<article class="media col-xs-12">' +
        '<div id="comJS-terms-body">' + options.termsText + '</div>' +
        '<div class="btn-toolbar">' +
        '<div class="btn-group"><button id="comJS-terms-agree" class="btn btn-primary" type="button">Agree</button></div>' +
        '<div class="btn-group"><button id="comJS-terms-disagree" class="btn btn-primary" type="button">Disagree</button></div>' +
        '</div>' +
        '</article>' +
        '</div>' +
        '</section>');

      $("#comJS-terms-agree").click(function () {
        $.cookie(options.agreedCookieName, 'agree');
        $('#comJS-terms').remove();
        options.onAgreed(true);
      });

      $("#comJS-terms-disagree").click(function () {
        $('#comJS-terms').remove();
        $(options.containerSelector).html('<section id="comJS-terms">' +
          '<div id="comJS-terms-title"><h2 tabindex="-1">Unable to Proceed</h2></div>' +
          '<div class="row">' +
          '<article class="media col-xs-12">' +
          '<div id="comJS-terms-body">' + options.disagreedText + '</div>' +
          '<div class="buttons">' +
          '<button id="comJS-terms-return" class="btn btn-primary" type="button">Terms and Conditions</button>' +
          '</div>' +
          '</article>' +
          '</div>' +
          '</section>');
        $("#comJS-terms-return").click(function () {
          $('#comJS-terms').remove();
          appClass.showTerms(options);
          $('#comJS-terms-agree').focus();
        }).focus();
        options.onDisagreed();
      });
    } else {
      options.onAgreed(false);
    }
  };

});
