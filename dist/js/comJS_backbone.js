//extending base backbone classes with some helpers
var ComJSModel = Backbone.Model.extend({
    get: function(key) {
        var keys = (key || '').split('.');
        var value = Backbone.Model.prototype.get.apply(this, [keys[0]]);
        if (keys.length == 1) {
            return value;
        }
        if (typeof value !== 'object') {
            return undefined;
        }
        for (var i = 1; i < keys.length && value != undefined; i++) {
            value = value[keys[i]];
        }
        return value;
    },
    set: function(key, val, options) {
        if(typeof key == 'object') {
            for(var k in key) {
                if (key.hasOwnProperty(k)) {
                    this._set(k, key[k], val);
                }
            }
        } else {
            this._set(key, val, options);
        }
        return this;
    },
    _set: function(key, val, options) {
        var keys = (key || '').split('.');
        if (keys.length == 1) {
          var object = this.get(key);
          if (object && (object instanceof ComJSCollection) && !(val instanceof ComJSCollection)) {
            object.set(val);
          } else {
            Backbone.Model.prototype.set.apply(this, arguments);
          }
        } else {
            var object = this.get(keys[0]);
            if (typeof object !== 'object') {
                object = {};
            } {
                object = _.clone(object);
            }
            var lastObject = object;
            for(var i = 1 ; i < keys.length - 1; i++) {
                if (lastObject[keys[i]] === undefined) {
                    lastObject[keys[i]] = {};
                }
                lastObject = lastObject[keys[i]];
            }
            var lastKey =  keys[keys.length - 1];
            var oldValue = lastObject[lastKey];
            lastObject[lastKey] = val;
            Backbone.Model.prototype.set.apply(this, [keys[0], object]);
        }
    },
    toJSON: function() {
      var json = Backbone.Model.prototype.toJSON.apply(this);
      for (var k in json) {
        if (json[k] && json[k].toJSON) {
          json[k] = json[k].toJSON();
        }
      }
      return json;
    }
});
var ComJSView = Backbone.View.extend({
    localId: function(s) {
        return (this.id || this._fallbackId()) + '_' + s;
    },
    localEl: function(s) {
        return this.$('#'+this.localId(s));
    },
    _fallbackId: function() {
      this._fallbackId_val = this._fallbackId_val || Math.random().toString().split('.')[1];
      return this._fallbackId_val;
    }
});
var ComJSCollection = Backbone.Collection.extend({ model: ComJSModel });
