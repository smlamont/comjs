const runSequence = require('run-sequence');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const remoteSrc = require('gulp-remote-src');
const babel = require('gulp-babel');
const rename = require('gulp-rename');
const useref = require('gulp-useref');
const gulpIf = require('gulp-if');
const uglify = require('gulp-uglify');
const cssnano = require('gulp-cssnano');
const htmlmin = require('gulp-htmlmin');
const cache = require('gulp-cache');
const notify = require('gulp-notify');
const imagemin = require('gulp-imagemin');
const inject = require('gulp-inject');
const preprocess = require('gulp-preprocess');
const git = require('gulp-git');
const replace = require('gulp-replace');
const del = require('del');
const browserSync = require('browser-sync').create();
const env = require('node-env-file');
const fs = require("fs");
const tap = require('gulp-tap');
const zip = require('gulp-zip');
const mergeStreams = require('merge-stream');
const awsPublish = require('gulp-awspublish');

/*
 See src/scaffolding/sample_gulpfile.js for how to use this file
 */
const GulpHelper = {

    /*
    createTasks
    This creates all the gulp tasks you need to build, run, and deploy a comJS app.

    gulp: your gulp instance
    options: a javascript key/value pair object. see initializer below for documentation
     */

    createTasks: (gulp, options) => {
      options = Object.assign({
        pkg: null, //Required for release task. the app's package.json file as a javascript object. when specified, appName and config can be omitted
        appName: (options['pkg'] || {})['name'], //Required unless pkg is specified. The name of the app. This is used to create build and distribution folders.
        config: (options['pkg'] || {})['comJSConfig'], //Required unless pkg is specified. The comJSConfig options from the app's package.json
        embedArea: 'full', //Optional. Only applies to embedded apps. Indicates where this app should be embedded in the simulator: 'full', 'left', or 'right'
        environmentOverride: null, //Optional, set to 'local', 'dev', 'qa', or 'prod' to force a build for a specified environment
        deploymentPath: '', //By default, standalone apps are built to deploy to /webapps/appname, but you can use this option to override that value. Note this should NOT end with a slash. ex: /webapps/work/decom/appname
        preprocessorContext: {}
        /*
         Developers can use the preprocessorContext option to add to the preprocessor context
         They can add environment specific items or generic items.
         ex:
         comJS.embeddedApp.createTasks(gulp, {
           ...
           preprocessorContext: {
             local: {
             ... these vars will only be in the context in the local environment ...
             },
             dev: {
             ... these vars will only be in the context in the local environment ...
             }
             OTHER: 'this var will be in the context of any environment'
           }
           ...
         });
         */
      }, options);

      if (!options['config'] || !options['appName']) {
        throw new Error('config and appName are both required for createTasks options');
      }

      let QA_RELEASE = (options.pkg || {})['qaRelease'];
      let VERSION = (options.pkg || {})['version'];
      let isEmbeddedApp = options['config']['isEmbedded'];
      let deploymentPath = isEmbeddedApp ? `/resources/${options['appName']}` : (options['deploymentPath'] || `/webapps/${options['appName']}`);

      //load env variables
      try {
        env('./.env');
      } catch (err) {
        process.stdout.write('Warning!!: no .env file found\n');
      }

      //what is the deployment environment?
      let deployment_environment = options['environmentOverride'] || 'local';

      function preprocessorOptions() {
        /*
         comJS uses https://www.npmjs.com/package/gulp-preprocess to allow for preprocessing

         See https://github.com/jsoverson/preprocess#directive-syntax for more about preprocessor syntax

         ex:
         // @if ENV='local' || ENV='dev'
         console.log('running on local or dev');
         // @endif

         Any variables inside your .env file will be available to preprocessor statements
         Available preprocessor variables:

         ENV
         //the current build environment: local, dev, qa, or prod

         SRC_PATH
         //the deployment path of your project, ex: /resources/app_name

         QA_RELEASE
         //the current QA release value (from package.json)

         VERSION
         //the current version value from package.json

         IS_EMBEDDED
         //a boolean indicating if the current app is embedded or not

         BUILD_ID
         //A unique ID for the current build

         startBuildTagWithCacheBuster
         //A helper method used in app.html, see usage/documentation there and below

         */
        let env = deployment_environment;
        let qaRelease = QA_RELEASE === undefined ? 'unavailable' : QA_RELEASE;
        let version = VERSION || 'unavailable';
        let BUILD_ID = new Date();
        BUILD_ID = [BUILD_ID.getFullYear(),BUILD_ID.getMonth()+1, BUILD_ID.getDate(), BUILD_ID.getHours(), BUILD_ID.getMinutes(), BUILD_ID.getSeconds()].join('-');
        // noinspection JSUnusedGlobalSymbols
        let opt = {
          context: {
            ENV: env,
            SRC_PATH: deploymentPath,
            QA_RELEASE: qaRelease,
            VERSION: version,
            IS_EMBEDDED: isEmbeddedApp,
            BUILD_ID,
            startBuildTagWithCacheBuster: function (file, params) {
              //file: Required. The name of the file to build. ex: 'scripts/main.css'
              //params: Optional. Additional params to go in file tag. ex: 'media="print"'
              let ext = file.split('.')[1];
              let path = file.split('.')[0];
              let cacheBust = env === 'prod' ? version : env === 'qa' ? qaRelease : env === 'dev' ? Math.random().toString().split('.')[1] : '';
              return `<!-- build:${ext} ${deploymentPath}/${path}${cacheBust}.${ext} ${params || ''} -->`;
            }
          }
        };

        if (options['preprocessorContext']) { //add user-defined options to the comJS created context
          for(let name in options['preprocessorContext']) {
            if (options['preprocessorContext'].hasOwnProperty(name)) {
              if (['local', 'dev', 'qa', 'prod'].indexOf(name) === -1) {
                opt.context[name] = options['preprocessorContext'][name];
              } else if (name === env) {
                Object.assign(opt.context, options['preprocessorContext'][name]);
              }
            }
          }
        }
        return opt;
      }

      let compress = false; //determines if files are minified, this is set to true in deploy tasks

      gulp.task('default', () => {
        return new Promise(resolve => {
          runSequence.use(gulp);
          runSequence('clean', 'build', resolve);
        });
      });

      //a task to remove the temp and dist directories
      gulp.task('clean', () => {
        del.sync(['.tmp', 'dist']);
      });

      //a task to build the project and create the distribution folder
      gulp.task('build', ['_html_styles_scripts', '_images', '_fonts', '_extras', '_bower_extras', '_data']);

      gulp.task('build_with_simulator', ['build'], () => {
        function processSimulatorSrc(stream) {
          let appHtml = gulp.src(['dist' + deploymentPath + '/html/app.html']);

          function between(string, start, end) {
            let x = string.indexOf(start);
            if (x > -1) {
              //noinspection JSUnresolvedFunction
              string = string.substring(x + start.length);
              let y = string.indexOf(end);
              if (y > -1) {
                //noinspection JSUnresolvedFunction
                return string.substring(0, y);
              }
            }
            return '';
          }

          return stream.pipe(rename((path) => {
            if (path.extname === '.html' || path.basename === 'cframe') {
              path.basename = 'index';
              path.extname = '.html'
            }
          }))
            .pipe(inject(appHtml, {
              starttag: '<!-- comJS_app_injection:head -->',
              endtag: '<!-- end_comJS_app_injection -->',
              transform: function (filePath, file) {
                return between(file.contents.toString('utf8'), '<!-- comJS-app:head -->', '<!-- comJS-app:head end-->');
              }
            }))
            .pipe(inject(appHtml, {
              starttag: '<!-- comJS_app_injection:body -->',
              endtag: '<!-- end_comJS_app_injection -->',
              transform: function (filePath, file) {
                let body = between(file.contents.toString('utf8'), '<!-- comJS-app:body -->', '<!-- comJS-app:body end-->');
                if (!isEmbeddedApp) {
                  return body;
                } else {
                  let html = '<div class="page-header"><h1>' + options['appName'] + ' Simulator</h1></div>';
                  switch (options['embedArea']) {
                    case 'left':
                      html += '<div class="row"><div id="page-content" class="col-md-8 col-lg-9">' + body + '</div>';
                      html += '<aside class="col-md-4 col-lg-3"></aside></div>';
                      break;
                    case 'right':
                      html += '<div class="row"><div id="page-content" class="col-md-8 col-lg-9"></div>';
                      html += '<aside class="col-md-4 col-lg-3">' + body + '</aside></div>';
                      break;
                    default:
                      html += '<div class="row"><div id="page-content" class="col-xs-12">' + body + '</div></div>';
                  }
                  return html;
                }
              }
            }))
            .pipe(inject(appHtml, {
              starttag: '<!-- comJS_app_injection:footer -->',
              endtag: '<!-- end_comJS_app_injection -->',
              transform: function (filePath, file) {
                return between(file.contents.toString('utf8'), '<!-- comJS-app:footer -->', '<!-- comJS-app:footer end-->');
              }
            }))
            .pipe(gulp.dest('dist'));
        }

        function staticStream() {
          return GulpHelper.inject(gulp, inject, gulp.src('./node_modules/@comJS_components/comJS/dist/html/cframe.html'), Object.assign(options['config'], {environment:deployment_environment}))
            .pipe(preprocess(preprocessorOptions()))
            .pipe(useref({newLine: '\n\n', searchPath: ['.']}))
            .pipe(gulpIf(compress, gulpIf('*.html', htmlmin({collapseWhitespace: false}))));
        }

		return processSimulatorSrc(!isEmbeddedApp ? staticStream() : remoteSrc(['cframe.html'], {
		  base: 'https://s3.ca-central-1.amazonaws.com/dev-com-resources/templates/',
          buffer: true,
          requestOptions: {
            proxy: process.env['PROXY'] 
          }
        }).on('error', function (e) {
			process.stdout.write('Error loading cframe: ' + e.toString() + '\nTrying public cFrame without proxy \n');
			processSimulatorSrc(remoteSrc(['cframe.html'], {
				base: 'https://s3.ca-central-1.amazonaws.com/dev-com-resources/templates/',
				buffer: true,
			}).on('error', function (e) {
				process.stdout.write('Error loading cframe: ' + e.toString() + '\nUsing barebones default simulator instead\n');
				processSimulatorSrc(gulp.src('./node_modules/@comJS_components/comJS/default_simulator.html')
				.pipe(useref({newLine: '\n\n', searchPath: ['.']})));
				//noinspection JSUnresolvedFunction
				this.emit('end');
			}));
			this.emit('end');
		}))

      });

      gulp.task('build_standalone', ['build_with_simulator'], () => {
        del.sync('dist' + deploymentPath + '/html/app.html', {force: true});
        return gulp.src('dist/index.html')
          .pipe(gulp.dest('dist' + deploymentPath));
      });

      gulp.task('run', () => {
        return new Promise(resolve => {
          runSequence('clean', 'build_with_simulator', '_serve', resolve);
        });
      });

      function copyToNetshare(env){
        let path = process.env['NETSHARE_DRIVE'];
        if (path) {
          path += `/web/content/${env || 'DEV'}${deploymentPath}`;
          process.stdout.write('Deleting existing app at ' + path + '\n');
          del.sync(path, {force: true});
          process.stdout.write('Copying app to ' + path + '.\n');
          if (env === 'STAGE4PROD') {
            process.stdout.write('App will be ready for double rsync to s3 production.\n');
          } else {
            process.stdout.write('rsync should move these changes to s3 within a few minutes.\n');
          }
          return gulp.src('dist' + deploymentPath + '/**/*.*')
            .pipe(gulp.dest(path));
        } else {
          process.stdout.write('Files are ready in dist' + deploymentPath + '.\n');
          process.stdout.write('To have this copied to a remote distribution automatically next time, do the following:\n');
          process.stdout.write('1. Insert the following line into your .env file: NETSHARE_DRIVE=??\n');
          process.stdout.write('2. Replace the ?? with the mapped drive letter and colon, ex: NETSHARE_DRIVE=z:\n');
          process.stdout.write('2.1 Note that on a Mac, this looks different. Something like: NETSHARE_DRIVE=/Volumes/inet \n');
        }
      }
      gulp.task('deploy:dev', ['_deploy_prep:dev'], () => {
        try {
          if (isEmbeddedApp) {
            return copyToAWS('dev');
          } else {
            let path = process.env['DEV_DEPLOY_PATH'];
            if (path) {
              del.sync(path, {force: true});
              process.stdout.write('Deploying to ' + path + '\n');
              return gulp.src('dist' + deploymentPath + '/**/*.*')
                .pipe(gulp.dest(path));
            } else {
              process.stdout.write('Files are ready in dist' + deploymentPath + '.\n');
              process.stdout.write(`You must now copy these files to your distribution environment.\n`);
              process.stdout.write('To have this copied automatically next time, do the following:\n');
              process.stdout.write('1. Make sure that you have a .env file in your project (should be in the root folder)\n');
              process.stdout.write('2. Insert the following line into your .env file: DEV_DEPLOY_PATH=??\n');
              process.stdout.write('3. Replace the ?? with a directory path to copy your dev distribution to.\n');
              process.stdout.write('3.1 Example: DEV_DEPLOY_PATH=z:\\some\\deployment\\path \n');
            }
          }
        } catch(e) {
          process.stdout.write('An error occurred: \n' + e.toString());
        }
      });

      function zipAndCopy(envVar){
        process.stdout.write('Packaging and zipping files.\n');
        let path = process.env[envVar];
        if (!path) {
          path = 'dist';
          process.stdout.write('The ZIP file will be place in dist, do with it what you will.\n');
          process.stdout.write('To have this copied somewhere automatically next time, do the following:\n');
          process.stdout.write('1. Make sure that you have a .env file in your project (should be in the root folder)\n');
          process.stdout.write(`2. Insert the following line into your .env file: ${envVar}=??\n`);
          process.stdout.write('3. Replace the ?? with a directory path to copy the ZIP file to.\n');
          process.stdout.write(`3.1 Example: ${envVar}=z:\\some\\deployment\\path \n`);
        } else {
          process.stdout.write('The ZIP file will be place in ' + path + ', do with it what you will.\n');
        }
        let now = new Date();
        let fileName = `${options['appName']}_${envVar.substring(0,envVar.lastIndexOf('_'))}_${now.getFullYear()}_${("0" + (now.getMonth() + 1)).slice(-2)}_${now.getDate()}_${("0" + now.getHours()).slice(-2)}${("0" + now.getMinutes()).slice(-2)}.zip`;
        return gulp.src('dist' + deploymentPath + '/**/*.*')
          .pipe(tap(function (file) {
            if (file.isDirectory()) {
              file.stat.mode = parseInt('40777', 8); //fix directory permissions when running on windows
            }
          }))
          .pipe(zip(fileName))
          .pipe(gulp.dest(path));
      }

      gulp.task('deploy:qa', ['_deploy_prep:qa'], () => {
        try {
          if (isEmbeddedApp) {
            return copyToAWS('qa');
          } else {
            return zipAndCopy('QA_DEPLOY_PATH');
          }
        } catch(e) {
          process.stdout.write('An error occurred: \n' + e.toString());
        }
      });

      gulp.task('deploy:prod', ['_deploy_prep:prod'], () => {
        try {
          if (isEmbeddedApp) {
            return copyToAWS('prod');
          } else {
            return zipAndCopy('PROD_DEPLOY_PATH');
          }
        } catch(e) {
          process.stdout.write('An error occurred: \n' + e.toString());
        }
      });

      gulp.task('release', () => {
        process.stdout.write('checking working copy for changes...\n');
        return git.status({args : '--porcelain'}, function (err, stdout) {
          if (stdout) {
            throw new Error('You cannot proceed until your working copy is clean!');
          } else {
            process.stdout.write('Working copy ok, proceeding\n');
            if (!options.pkg) {
              throw new Error('To use the release task, you must pass in the pkg option to gulp_helper#embeddedApp.createTasks');
            }
            let prod = process.argv.indexOf('--prod') > -1 ? 1 : 0;
            let qa = process.argv.indexOf('--qa') > -1 ? 1 : 0;
            if (prod + qa !== 1) {
              throw new Error('Invalid arguments, specify --qa or --prod\n');
            }
            let deployTask = `deploy:${prod ? 'prod' : 'qa'}`;
            return new Promise(resolve => {
              runSequence('_increment_release_number', deployTask, '_tag_release', resolve);
            });
          }
        });
      });

      gulp.task('_increment_release_number', () => {
        let qa = process.argv.indexOf('--qa') > -1 ? 1 : 0;
        if (qa) {
          let oldNumber = QA_RELEASE || 0;
          let propString = `"qaRelease": ${oldNumber},`;
          if(fs.readFileSync('./package.json', 'utf8').indexOf(propString) === -1) {
            throw new Error(`The qaRelease property in your package.json file is missing or has bad whitespace. It should look like: ${propString}`);
          }
          QA_RELEASE = oldNumber + 1;
          process.stdout.write(`Updating qaRelease from ${oldNumber} to ${QA_RELEASE}\n`);
          return gulp.src('./package.json')
            .pipe(replace(propString, `"qaRelease": ${QA_RELEASE},`))
            .pipe(gulp.dest('./'))
            .pipe(git.commit(`Updated qaRelease property to ${QA_RELEASE}`));
        } else {
          let major = process.argv.indexOf('--major') > -1 ? 1 : 0;
          let minor = process.argv.indexOf('--minor') > -1 ? 1 : 0;
          let patch = process.argv.indexOf('--patch') > -1 ? 1 : 0;
          if (major + minor + patch !== 1) {
            throw new Error('Invalid arguments, specify --major or --minor or --patch');
          }
          let oldVersion = VERSION || 'x.x.x';
          let propString = `"version": "${oldVersion}",`;
          if(fs.readFileSync('./package.json', 'utf8').indexOf(propString) === -1) {
            throw new Error(`The version property in your package.json file is missing or has bad whitespace. It should look like: ${propString}`);
          }
          let a = oldVersion.split('.');
          let i = major ? 0 : minor ? 1 : 2;
          a[i] = (parseInt(a[i]) + 1).toString();
          while (++i < 3) {
            a[i] = '0';
          }
          VERSION = a.join('.');
          process.stdout.write(`Updating version from ${oldVersion} to ${VERSION}\n`);
          return gulp.src('./package.json')
            .pipe(replace(propString, `"version": "${VERSION}",`))
            .pipe(gulp.dest('./'))
            .pipe(git.commit(`Updated version property to ${VERSION}`));
        }
      });

      gulp.task('_tag_release', () => {
        let prod = process.argv.indexOf('--prod') > -1 ? 1 : 0;
        let qa = process.argv.indexOf('--qa') > -1 ? 1 : 0;
        if (prod + qa !== 1) {
          throw new Error('Invalid arguments, specify --qa or --prod\n');
        }
        let tag = prod ? 'r' + VERSION : 'qa' + QA_RELEASE;
        let comment = prod ? 'Release ' + VERSION : 'QA Release ' + QA_RELEASE;
        git.tag(tag, comment, function (err) {
          if (err) throw err;
        });
        process.stdout.write(`HEY! Don't forget to push this release to your git repo!!!:\n\ngit push && git push --tags\n\n`);
      });

      //preprocess syntax: https://github.com/jsoverson/preprocess#directive-syntax
      gulp.task('_html_styles_scripts', ['_styles', '_scripts'], () => {
        return GulpHelper.inject(gulp, inject, gulp.src(['src/*.html']), Object.assign(options['config'], {environment:deployment_environment}))
          .pipe(preprocess(preprocessorOptions()))
          .pipe(useref({newLine: '\n', searchPath: ['.tmp', 'src', '.']}))
          .pipe(gulpIf(compress, gulpIf('*.js', uglify())))
          .pipe(gulpIf(compress, gulpIf('*.css', cssnano({safe: true}))))
          .pipe(gulpIf(compress, gulpIf('*.html', htmlmin({collapseWhitespace: false}))))
          .pipe(rename((path) => {
            if (path.extname === '.html') {
              path.dirname = deploymentPath.substr(1) + '/html';
            }
          }))
          .pipe(gulp.dest('dist'));
      });

      gulp.task('_styles', () => {
        //noinspection JSUnresolvedVariable
        return gulp.src('src/styles/**/*.scss')
          .pipe(preprocess(preprocessorOptions()))
          .pipe(sass.sync({
            outputStyle: 'expanded',
            precision: 10,
            includePaths: ['.']
          }).on('error', sass.logError))
          .pipe(autoprefixer({browsers: ['> 2% in CA', 'last 2 versions']}))
          .pipe(gulp.dest('.tmp' + deploymentPath + '/styles'));
      });

      gulp.task('_scripts', () => {
        return gulp.src('src/scripts/**/*.js')
          .pipe(preprocess(preprocessorOptions()))
          .pipe(babel({presets: [['env',{targets:{browsers:['> 2% in CA','last 2 versions']}}]]})).on("error", notify.onError(function (error) {
            let msg = error.message;
            if (msg.indexOf('Couldn\'t find preset "env"') > -1) {
              msg += '\n\n!!!!!\nMake sure you\'ve installed the babel-preset-env npm package with: npm install babel-preset-env --save-dev\n!!!!!\n\n';
            }
            return msg;
          }))
          .pipe(gulp.dest('.tmp' + deploymentPath + '/scripts'));
      });

      gulp.task('_images', () => {
        return gulp.src(['src/img/**/*'])
          .pipe(gulpIf(compress, cache(imagemin())))
          .pipe(gulp.dest('dist' + deploymentPath + '/img'));
      });

      gulp.task('_fonts', () => {
        return gulp.src(['src/fonts/**/*'])
          .pipe(gulp.dest('dist' + deploymentPath + '/fonts'));
      });

      gulp.task('_data', () => {
        //override this in your project if you want to do extra stuff during the build task

        //FAKING DATA AND CONFIG FILES:
        //Some apps will load data and configurations from 'data' files, usually JSON.
        //The location of these files in Dev, QA, and Prod may vary:
        //- Could be in the S3 data bucket
        //- Could be in a custom Wordpress post
        //To 'fake' this when running your app locally, do the following:
        // 1. Put some sample data/config files into /src/data folder in the project
        // 2. Overwrite the _data gulp task to copy your data files into a file path that mimics the one on your web server:
        // gulp.task('_data', () => {
        //   let myDataPath = '/data'; //On S3, this will be something like /data/division/my_app
        //                             //On WP, this will be /api_content
        //   return gulp.src(['src/data/**/*']).pipe(gulp.dest('dist' + myDataPath));
        // });
      });

      gulp.task('_extras', () => {
        return gulp.src([
          'src/*.*',
          '!src/*.html'
        ], {
          dot: true
        }).pipe(gulp.dest('dist' + deploymentPath));
      });

      gulp.task('_bower_extras', () => {
        return GulpHelper.distExtras(gulp, 'dist' + deploymentPath, options['config']);
      });

      gulp.task('_serve', () => {
        gulp.watch('src/fonts/**/*', ['_fonts']);
        gulp.watch('src/img/**/*', ['_images']);
        gulp.watch('src/data/**/*', ['_data']);
        gulp.watch(['src/*.*', '!src/*.html'], ['_extras']);
        gulp.watch(['src/scripts/**/*.js', 'src/styles/**/*.scss'], ['_html_styles_scripts']);
        gulp.watch(['src/*.html'], ['build_with_simulator']);
        browserSync.init({
          port: 9000,
          server: {
            baseDir: ['dist']
          }
        });
        //could get fancy here and use the stream method for things like CSS, to avoid reloading the whole page
        gulp.watch('dist/**/*').on('change', browserSync.reload);
      });

      //a task to clear the gulp-cache in case images get stuck in there
      gulp.task('_clear_image_cache', (done) => {
        return cache.clearAll(done);
      });

      function deployPrep(e, c) {
        deployment_environment = options['environmentOverride'] || e;
        compress = c;
        return new Promise(resolve => {
          runSequence('_clear_image_cache', 'clean', isEmbeddedApp ? 'build' : 'build_standalone', resolve);
        });
      }

      gulp.task('_deploy_prep:dev', () => {
        return deployPrep('dev', false);
      });

      gulp.task('_deploy_prep:qa', () => {
        return deployPrep('qa', true);
      });

      gulp.task('_deploy_prep:prod', () => {
        if (options['environmentOverride']) {
          process.stdout.write('\n\nWARNING! You are deploying to production with an environment override.\nThis is not allowed, and your override is being ignored.\n\n');
          options['environmentOverride'] = null; //do not allow environment overrides when deploying to production
        }

        return deployPrep('prod', true);
      });
	  
	  //a task to deploy the project to AWS S3
	function uploadToS3(region, bucket, path, proxy_addr) {
 
		// create a new publisher using S3 options
		// http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#constructor-property
		let httpOpts = proxy_addr === "" ? {} :  {proxy:proxy_addr};
		let awsConfig = {
			region : region,
			params: {
			Bucket : bucket
		},
		httpOptions : httpOpts };
		let publisher = awsPublish.create(awsConfig,{});

		// optionally define custom headers (pass to publish method below)
		// let headers = {
		//   'Cache-Control': 'max-age=315360000, no-transform, public'
		//   // ...
		// };
		return gulp.src('dist' + deploymentPath + '/**/*.*')

			.pipe(rename((p) => {
			p.dirname = path + p.dirname; //rename to the desired s3 path
		}))

		// publisher will add Content-Length, Content-Type and headers specified above
		// If not specified it will set x-amz-acl to public-read by default
		.pipe(publisher.publish())

		//sync to delete files from s3 that are not in the dist folder.
		//dirname is important, otherwise this will delete everything in the bucket except this app
		.pipe(publisher.sync(path,[]))

		// print upload updates to console
		.pipe(awsPublish.reporter({}));
	}

	//gulp.task('deploy:aws', ['_deploy_prep:dev'], () => {
		function copyToAWS(env){
		//AWS credentials are stored in ~/.aws/credentials
		//see http://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/loading-node-credentials-shared.html
		let awsRegion = options.config.awsRegion;
		let awsBucket = options.config['awsBucket-' + env];
		let awsDeployPath = options.config.awsDeployPath;
		let proxy = process.env['PROXY'];
		return uploadToS3(awsRegion, awsBucket, awsDeployPath, proxy);
	};


},
  inject: (gulp, inject, stream, options) => {
    //gulp is your gulp object
    //inject is your gulp-inject object
    //stream is the result of a call to gulp.src
    //options is an object to specify what to inject
    //  see the 'comJSConfig properties' section of package_settings.md for all the options here

    if (options['isEmbedded']) { //some things are not allowed for embedded apps
      options['includeLogin'] = false;
    }

    let shivFiles = options['isEmbedded'] ? [] : [
      './node_modules/@comJS_components/html5shiv/dist/html5shiv.js',
      './node_modules/@comJS_components/respond/src/respond.js'
    ];

    let comJSFiles = [
      './node_modules/@comJS_components/jquery.cookie/jquery.cookie.js'
    ];

    let robotoFiles = options['isEmbedded'] ? [] : [
      '/.node_modules/@comJS_components/roboto-fontface/css/roboto/roboto-fontface.css'
    ];
    let comJSPrintFiles = [];

    if (options['includeModeling']) {
      process.stdout.write('comJS gulp helper injecting underscore/backbone for modeling\n');
      comJSFiles = comJSFiles.concat([
        "./node_modules/@comJS_components/underscore/underscore.js",
        "./node_modules/@comJS_components/backbone/backbone.js",
        "./node_modules/@comJS_components/comJS/dist/js/comJS_backbone.js"
      ]);
    }
    if (options['includeIntlTelInput']) {
      process.stdout.write('comJS gulp helper injecting intlTelInput\n');
      comJSFiles = comJSFiles.concat([
        "./node_modules/@comJS_components/intl-tel-input/build/js/intlTelInput.js",
        "./node_modules/@comJS_components/intl-tel-input/build/css/intlTelInput.css"
      ]);
    }
    if (options['includeJQueryMaskedInput']) {
      process.stdout.write('comJS gulp helper injecting jquery.maskedinput.js\n');
      comJSFiles = comJSFiles.concat([
        "./node_modules/@comJS_components/jquery.maskedinput/dist/jquery.maskedinput.js",
      ]);
    }
	
	//sml: with anyform add in css (rename this css and remove embedded
    if (options['includeFormValidation']) {
      process.stdout.write('comJS gulp helper injecting comJS_forms and formValidation\n');
      comJSFiles = comJSFiles.concat([
        "./node_modules/@comJS_components/comJS/dist/js/formValidation.min.js",
        "./node_modules/@comJS_components/comJS/dist/js/comJS_forms.js",
        "./node_modules/@comJS_components/comJS/dist/css/formValidation.min.css",
		'./node_modules/@comJS_components/comJS/dist/css/embedded_comJS_forms.css'
      ]);
    //  if (options['isEmbedded']) {
     //   comJSFiles = comJSFiles.concat([
     //     './node_modules/@comJS_components/comJS/dist/css/embedded_comJS_forms.css'
    //    ]);
     // }
    }
    if (options['includeEditableSelect']) {
      process.stdout.write('comJS gulp helper injecting jquery-editable-select\n');
      comJSFiles = comJSFiles.concat([
        "./node_modules/@comJS_components/jquery-editable-select/dist/jquery-editable-select.js",
        "./node_modules/@comJS_components/jquery-editable-select/dist/jquery-editable-select.css"
      ]);
    }
    if (options['includePlaceholders']) {
      process.stdout.write('comJS gulp helper injecting jquery placeholders\n');
      comJSFiles = comJSFiles.concat(['./node_modules/@comJS_components/placeholders/dist/placeholders.jquery.js']);
    }
    if (options['includeMultiSelect']) {
      process.stdout.write('comJS gulp helper injecting bootstrap-multiselect\n');
      comJSFiles = comJSFiles.concat([
        './node_modules/@comJS_components/bootstrap-multiselect/dist/js/bootstrap-multiselect.js',
        './node_modules/@comJS_components/comJS/dist/js/comJS_multiselect.js',
        './node_modules/@comJS_components/bootstrap-multiselect/dist/css/bootstrap-multiselect.css'
      ]);
    }
    if (options['includeOMS']) {
      process.stdout.write('comJS gulp helper injecting OMS for google maps\n');
      comJSFiles = comJSFiles.concat(['./node_modules/@comJS_components/comJS/dist/js/oms.min.js']);
    }
    if (options['includeMoment'] || options['includeFullCalendar'] || options['includeDatePicker'] || options['includeRangePicker']) {
      process.stdout.write('comJS gulp helper injecting momentjs\n');
      comJSFiles = comJSFiles.concat(['./node_modules/@comJS_components/moment/min/moment-with-locales.js']);
    }
    if (options['includeFullCalendar']) {
      process.stdout.write('comJS gulp helper injecting fullcalendar\n');
      comJSFiles = comJSFiles.concat(['./node_modules/@comJS_components/fullcalendar/dist/fullcalendar.js']);
      comJSFiles = comJSFiles.concat(['./node_modules/@comJS_components/fullcalendar/dist/fullcalendar.css']);
      comJSPrintFiles = comJSPrintFiles.concat(['./node_modules/@comJS_components/fullcalendar/dist/fullcalendar.print.css']);
    }
    if (options['includeDatePicker']) {
      process.stdout.write('comJS gulp helper injecting bootstrap-datetimepicker\n');
      comJSFiles = comJSFiles.concat([
        './node_modules/@comJS_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
        './node_modules/@comJS_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css'
      ]);
    }
    if (options['includeLogin']) {
      process.stdout.write('comJS gulp helper injecting comJS_login\n');
      comJSFiles = comJSFiles.concat(['./node_modules/@comJS_components/comJS/dist/js/comJS_login.js']);
    }
    if (options['includeRangePicker']) {
      process.stdout.write('comJS gulp helper injecting daterangepicker\n');
      comJSFiles = comJSFiles.concat([
        './node_modules/@comJS_components/bootstrap-daterangepicker/daterangepicker.js',
        './node_modules/@comJS_components/bootstrap-daterangepicker/daterangepicker.css'
      ]);
    }
    if (options['includeBootbox']) {
      process.stdout.write('comJS gulp helper injecting bootbox\n');
      comJSFiles = comJSFiles.concat([
        './node_modules/@comJS_components/bootbox.js/bootbox.js',
      ]);
    }
    if (options['includeDropzone']) {
      process.stdout.write('comJS gulp helper injecting dropzone\n');
      comJSFiles = comJSFiles.concat([
        './node_modules/@comJS_components/dropzone/dist/dropzone.js',
        './node_modules/@comJS_components/comJS/dist/js/comJS_dropzone.js',
        './node_modules/@comJS_components/dropzone/dist/dropzone.css'
      ]);
    }
    if (options['includeModal'] || options['includeLogin']) {
      process.stdout.write('comJS gulp helper injecting comJS_modal\n');
      comJSFiles = comJSFiles.concat([
        './node_modules/@comJS_components/comJS/dist/js/comJS_modal.js',
      ]);
    }
    if (options['includeTerms']) {
      process.stdout.write('comJS gulp helper injecting comJS_terms\n');
      comJSFiles = comJSFiles.concat([
        './node_modules/@comJS_components/comJS/dist/js/comJS_terms.js',
      ]);
    }
    if (options['isEmbedded']) {
      comJSFiles = comJSFiles.concat([
        './node_modules/@comJS_components/comJS/dist/js/embedded_comJS_app.js'
      ]);
    } else {
      comJSFiles = comJSFiles.concat([
        './node_modules/@comJS_components/comJS/dist/js/comJS_app.js',
        //'./node_modules/@comJS_components/comJS/dist/css/comJS_app.css'
      ]);
    }

    return stream
      .pipe(inject(gulp.src(shivFiles, {read: false}), {name: 'shiv', relative: false}))
      .pipe(inject(gulp.src(comJSFiles, {read: false}), {name: 'comJS', relative: false}))
      .pipe(inject(gulp.src(robotoFiles, {read: false}), {name: 'roboto', relative: false}))
      .pipe(inject(gulp.src(comJSPrintFiles, {read: false}), {
        name: 'comJS_print',
        relative: false,
        transform: function (filePath) {
          if (filePath.slice(-4) === '.css') {
            return '<link rel="stylesheet" type="text/css" media="print" href="' + filePath + '">';
          }
          // Use the default transform as fallback:
          return inject.transform.apply(inject.transform, arguments);
        }
      }));
  },
  distExtras: (gulp, distDir, options) => {
    //gulp is your gulp object
    //distDir is your distribution directory
    //options is the same as the inject method above

    let streams = [];
    //bootstrap needs font files:
    if (!options['isEmbedded']) {
      streams.push(gulp.src(['./node_modules/@comJS_components/bootstrap/dist/fonts/*']).pipe(gulp.dest(distDir + '/fonts')));
    }

    if (!options['isEmbedded']) {
      //Roboto needs font files:
      streams.push(gulp.src(['./node_modules/@comJS_components/roboto-fontface/fonts/Roboto/*']).pipe(gulp.dest(distDir + '/fonts/Roboto')));
    }
    //comJS and intl-tel-input need images:
    let imageSrcs = options['isEmbedded'] ? [] : ['./node_modules/@comJS_components/comJS/dist/img/*'];
    if (options['includeIntlTelInput']) {
      imageSrcs.push('./node_modules/@comJS_components/intl-tel-input/build/img/*');
    }
    if (imageSrcs.length > 0) {
      streams.push(gulp.src(imageSrcs).pipe(gulp.dest(distDir + '/img')));
    }

    //intl-tel-input needs a weird lib build folder with a js util referenced by the comJS
    if (options['includeIntlTelInput']) {
      streams.push(gulp.src('./node_modules/@comJS_components/intl-tel-input/build/js/utils.js')
        .pipe(gulp.dest(distDir + '/js')));
    }

    return streams.length > 0 ? mergeStreams.apply(null, streams) : false;
  },
 };
module.exports = GulpHelper;
