const gulp = require('gulp');
const plugins = require('gulp-load-plugins')();
const del = require('del');
const runSequence = require('run-sequence');

function concatAndMinify(glob, dest, fileName, minify, beforeConcat) {
  return gulp.src(glob)
    .pipe(plugins.if(beforeConcat !== undefined, beforeConcat || function () {
      }))
    .pipe(plugins.concat(fileName))
    .pipe(gulp.dest(dest))
    .pipe(minify)
    .pipe(plugins.rename((path) => {
      path.extname = '.min' + path.extname;
    }))
    .pipe(gulp.dest(dest));
}

gulp.task('styles', () => {
  let distDir = 'dist/css';
  
  /* SML: no longer need this.. we'll use COM css. renvoe from gulphelper.
    concatAndMinify(['src/css/bootstrap_comJS_customizations.css', 'src/css/comJS_app.css'],
  */
  concatAndMinify([],
    distDir,
    'comJS_app.css',
    plugins.cssnano({safe: true}),
    plugins.autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']})
  );

  concatAndMinify(['src/css/embedded_comJS_forms.css'],
    distDir,
    'embedded_comJS_forms.css',
    plugins.cssnano({safe: true}),
    plugins.autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']})
  );
   concatAndMinify(['src/js/search/search.css'],
    distDir,
    'search.css',
    plugins.cssnano({safe: true}),
    plugins.autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']})
  ); 
	

  //unavailable for use with bower: http://formvalidation.io/
  return gulp.src('src/css/formValidation.min.css')
    .pipe(gulp.dest(distDir));

});

gulp.task('scripts', () => {
  let distDir = 'dist/js';
  concatAndMinify(['src/js/bootstrap_comJS_customizations.js', 'src/js/comJS_app.js'],
    distDir,
    'comJS_app.js',
    plugins.uglify()
  );

  //unavailable for use with bower: https://github.com/jawj/OverlappingMarkerSpiderfier
  gulp.src('src/js/oms.min.js')
    .pipe(gulp.dest(distDir));
	
  gulp.src('src/js/search/*.js')
    .pipe(gulp.dest(distDir));	

  //unavailable for use with bower: http://formvalidation.io/
  gulp.src(['src/js/formValidation.min.js', 'src/js/framework/bootstrap.min.js'])
    .pipe(plugins.concat('formValidation.min.js'))
    .pipe(gulp.dest(distDir));

  ['comJS_backbone', 'comJS_dropzone', 'embedded_comJS_app', 'comJS_forms', 'comJS_multiselect', 'comJS_modal', 'comJS_terms'].forEach(function(s){
    concatAndMinify(`src/js/${s}.js`,
      distDir,
      `${s}.js`,
      plugins.uglify()
    );
  });

});

gulp.task('html', () => {
  return gulp.src('src/html/*.html')
    .pipe(gulp.dest('dist/html'));
});

gulp.task('images', () => {
  return gulp.src('src/img/**/*')
    .pipe(plugins.cache(plugins.imagemin()))
    .pipe(gulp.dest('dist/img'));
});

//use these from the CLI:
gulp.task('clean', () => {
  del.sync(['dist']);
});

gulp.task('build', ['html', 'styles', 'scripts', 'images']);

gulp.task('default', () => {
  return new Promise(resolve => {
    runSequence(['clean'], 'build', resolve);
  });
});

// This is a task to scaffold a new core app project that uses npm, bower, gulp, etc. And loads the core with bower
//OPTIONS:
//-dir <path>   #Required, the local file path where the app will be created
//--embedded OR --standalone  #Required, indicate whether to create a standalone or embedded app
//--bare #Optional, if present then a bare app is created without any sample code

//EXAMPLES:
//gulp scaffold -dir /Library/WebServer/Documents/comJS/my_app_name --embedded #embedded app on a mac
//gulp scaffold -dir c:\path\to\my_app_name --standalone --bare #an empty standalone app on windows
//

gulp.task('scaffold', () => {
  let dirIndex = process.argv.indexOf('-dir');
  let isEmbedded = process.argv.indexOf('--embedded') > -1 || process.argv.indexOf('--e') > -1 ? 1 : 0;
  let isStandalone = process.argv.indexOf('--standalone') > -1 || process.argv.indexOf('--s') > -1 ? 1 : 0;
  let isBare = process.argv.indexOf('--bare') > -1;

  if ((isEmbedded + isStandalone !== 1) || dirIndex === -1 || dirIndex + 1 > process.argv.length - 1) {
    process.stdout.write('\n!!!YOU MISSED SOME OPTIONS (see below)!!!\n');
    process.stdout.write('\ngulp scaffold\n-------------\n');
    process.stdout.write('This is a task to scaffold a new core app project.\n\n');
    process.stdout.write('OPTIONS:\n');
    process.stdout.write('-dir \nRequired, the local file path where the app will be created\n');
    process.stdout.write('Note that the last part of the dir argument will be used as your app name\n');
    process.stdout.write('\n--embedded OR --standalone \nRequired, indicate whether to create a standalone or embedded app\n');
    process.stdout.write('You can also use short versions --e OR --s\n');
    process.stdout.write('\n--bare\nOptional, specify this to create a project that is completely empty with no demo code\n');
    process.stdout.write('\nEXAMPLES:\n');
    process.stdout.write('#create an embedded app called my_app_name:\n');
    process.stdout.write('gulp scaffold -dir c:\\path\\to\\my_app_name --embedded\n');
    process.stdout.write('\n#create a standalone app via relative path without any sample code:\n');
    process.stdout.write('gulp scaffold -dir ..\\my_app_name --standalone --bare\n');
    process.stdout.write('\n\n');
    return;
  }

  let dir = process.argv[dirIndex + 1];
  let name = dir.indexOf('/') !== -1 ? dir.split('/') : dir.indexOf('\\') !== -1 ? dir.split('\\') : [dir];
  if (!name[name.length - 1]) { //the path could end in \\ or /, causing a blank last array item
    name.pop();
  }

  name = name.length > 0 ? name[name.length - 1] : '';
  if (name.indexOf(' ') !== -1) {
    throw new Error('No spaces in the app name please');
  }
  if (!name) {
    throw new Error('Invalid dir specified');
  }
  process.stdout.write(`\nCreating a${isEmbedded ? 'n embedded' : ' standalone'} app named "${name}" in dir ${dir}\n\n`);
  return gulp.src([
    `src/scaffolding${isBare ? '_bare' : ''}/**/*`
  ], {dot: true})
    .pipe(plugins.replace('my_app_name', name))
    .pipe(plugins.if('sample_package.json',plugins.replace('"isEmbedded": 1,',`"isEmbedded": ${isEmbedded ? 'true' : 'false'},`)))
    .pipe(plugins.rename((p) => {
      if (p.basename.indexOf('sample_') !== -1) {
        p.basename = p.basename.split('sample_').join('');
      }
    }))
    .pipe(gulp.dest(dir));
});
