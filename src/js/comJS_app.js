var comJS_app = function(sName, options) {
    this.name = sName;
    //configurable options:
    $.extend(this,{
        hasHeader: true, //set to false to hide the main corporate header portion of the app interface
        hasFooter: true, //set to false to hide the main corporate footer portion of the app interface
        hasContentTop: true, //set to false if you don't need this portion of the content area
        hasContentBottom: true, //set to false if you don't need this portion of the content area
        hasContentRight: true, //set to false if you don't need this portion of the content area
        hasContentLeft: true, //set to false if you don't need this portion of the content area
        hasLeftNav: false, //set to true if you want to have a left hand navigation area
        searchcontext: 'INTER' //set the search context of this app, allowable values: 'INTER', 'INTRA', '311'
    }, options || {});

    this.breadcrumbItems = [];
    this.isRendered = false;
};

comJS_app.prototype.setTitle = function(title) {
    $("#app-header").find("h1").html(title);
};

comJS_app.prototype.setBreadcrumb = function (items, excludeAppName) {
	//items: an array of raw javascript objects that define breadcrumb items, supported properties are:
	    //link: if specified, the breadcrumb item will be a link to this URL
        //      NOTE: the last item is never a link
        //name: required, the title of the breadcrumb item
    //excludeAppName: By default, the app name is automatically set as the last breadcrumb item. Set this to true if you do not want that to happen

    this.breadcrumbItems = items;
    if (!excludeAppName) {
        this.breadcrumbItems.push({name: this.name});
    }
    this._renderBreadcrumb();
    return this;
};

comJS_app.prototype._renderBreadcrumb = function() {
    var container = $("#app-breadcrumb");
    if (container.length) {
        if (this.breadcrumbItems.length) {
          var rootUrl = this.searchcontext === 'INTER' ? 'https://www.mississauga.ca' : 'http://inside.mississauga.ca';
          var rootTitle = this.searchcontext === 'INTER' ? 'City of Mississauga' : 'InsideCOM';
            var itemsHtml = '<li><a href="' + rootUrl + '"><div class="glyphicon glyphicon-home" style="margin-right: 5px;"></div>' + rootTitle + '</a></li>';
            var lastIndex = this.breadcrumbItems.length - 1;
            $.each(this.breadcrumbItems, function(i,item) {
                if (item.name) {
                    var link = item.link && i < lastIndex ? '<a href="' + item.link + '">' + item.name + '</a>' : item.name;
                    itemsHtml += '<li>' + link + '</li>';
                }
            });
            container.find('ul').html(itemsHtml);
            container.show();
        } else {
            container.hide();
        }
    }
};

comJS_app.prototype.render = function() {
  if (this.isRendered) {
    throw new Error('App is already rendered');
  }

	var app=this;
  app.setTitle(app.name);
  app._renderBreadcrumb();

  //SET UP SEARCH WITH THE PROPER INTER CONTEXT
  switch (app.searchcontext) {
    case "INTRA":
      //SET UP SEARCH WITH THE PROPER 311 INTRA CONTEXT
      $("#siteSearchGSA").attr("action", "http://inside-search.mississauga.ca/search");
      $("#siteSearchGSA #q").attr("placeholder", "Search Inside Mississauga...");
      $("#sf_client").val("insideto-search");
      $("#sf_proxystylesheet").val("insideto-search");
      $("#sf_site").val("InsideTO-All");
      $(".sf_311").remove();
      break;

    case "311":
      //SET UP SEARCH WITH THE PROPER 311 INTRA CONTEXT
      $("#siteSearchGSA").attr("action", "http://inside-search.mississauga.ca/search");
      $("#siteSearchGSA #q").attr("placeholder", "Search the 311 Knowledge Base...");
      $("#sf_client").val("311KBSTAFF");
      $("#sf_proxystylesheet").val("311KBSTAFF");
      $("#sf_site").val("311KB");
      break;

    default:
      $("#siteSearchGSA").attr("action", "https://find.mississauga.ca/searchblox/servlet/SearchServlet");
      $("#siteSearchGSA #q").attr("placeholder", "");
      $("#siteSearchGSA #q").attr("name","query");
      $("#siteSearchGSA [name='mss']").remove();
      $("#siteSearchGSA [name='oe']").remove();
      $("#siteSearchGSA [name='ie']").remove();
      $("#siteSearchGSA [name='output']").remove();
      $("#siteSearchGSA [name='sort']").remove();

      $("#sf_client").remove();
      $("#sf_proxystylesheet").remove();
      $("#sf_site").remove();
      $(".sf_311").remove();

  }

  if (app.hasLeftNav) {
    $("#app-nav-left").removeClass("hide");
    $("#app-content-full").addClass("col-sm-9");
  } else {
    $("#app-nav-left").remove();
  }
  if(!app.hasHeader) {$("#comJS-header").remove();$("#app-header").remove();}
  if(!app.hasFooter) {$("#app-footer").remove();}
  if(!app.hasContentTop) {$("#app-content-top").remove();}
  if(!app.hasContentRight) {
    $("#app-content-left").removeClass('col-md-8');
    $("#app-content-right").remove();
  }
  if(!app.hasContentLeft) {$("#app-content-left").remove();}
  if(!app.hasContentBottom) {$("#app-content-bottom").remove();}
 //SML -- remvoe customizzations applyFontSize();
  $("#appDisplay").removeClass("hide");
  app.isRendered = true;
	return this;
};


/*
A convenience method for writing HTML into the app content area.
options: a javascript object with one or more of the keys 'top', 'right', 'bottom', 'left', 'nav' set to an HTML string to insert into that content area
 ex: {top: '<div>some html for the top area</div>'}
 */
comJS_app.prototype.setContent = function (options) {
    var app = this;
    $.each(['top', 'right', 'bottom', 'left', 'nav'], function (i, value) {
        if (options[value] !== undefined) {
            $(app.getContentContainerSelector(value)).html(options[value]);
        }
    });
};

/*
A convenience method for inserting a comJS_form object into the app content area.
comJSForm: a ComJSForm object
 area: a string with one of five possible values: 'top', 'right', 'bottom', 'left', 'nav'
replaceCurrentContent: defaults to true, specify false if the form should be appended into the content instead of replacing it
 */
comJS_app.prototype.addForm = function (comJSForm, area, replaceCurrentContent) {
    replaceCurrentContent = (replaceCurrentContent === undefined) ? true : replaceCurrentContent;

    if (replaceCurrentContent) {
        var clear = {};
        clear[area] = '';
        this.setContent(clear);
    }
    comJSForm.render({
        target: this.getContentContainerSelector(area)
    });
};

/*
A convenience method for getting the jquery selector of a given app content area
area: a string with one of five possible values: 'top', 'right', 'bottom', 'left', 'nav'
 */
comJS_app.prototype.getContentContainerSelector = function (area) {
    return area === 'nav' ? '#app-nav-left' : '#app-content-' + area;
};


