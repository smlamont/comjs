var IEversion = (navigator.userAgent.indexOf("MSIE") > 1) ? parseInt(navigator.userAgent.substr(navigator.userAgent.indexOf("MSIE") + 5, 5)) : 99;

/*
You can build forms with comJS_form, but it is preferable to use ComJSForm (lower down in this file).
You shouldn't really use comJS_form or any of its methods unless you really know what you are doing.
 */
var comJS_form = function (o) {
  this.id = o.id; //the id to assign this form. this is used for the associated HTML form element id
  this.title = o.title; //if specified, an H2 is added to the top of the form with this text in it
  this.success = o.success; //a function to call after form validation passes

  //the absolute url path to the application's root folder, required when using any validationtype=Phone fields
  // embedded example: '/resources/app_name/'
  // standalone example: '/webapps/app_name/'
  this.rootPath = o.rootPath;

  this.sections = [];
};

comJS_form.addDefaultFieldProperties = function (fields) {
  comJS_form.fixClassProperty(fields);
  (fields || []).forEach(function (fld) {
    fld.type = fld['type'] || 'text';
    fld.id = fld['id'] || Math.random().toString().split('.')[1];
    if ('html button static'.indexOf(fld.type) === -1 && !fld['title']) {
      console.warn('Missing title attribute for field ' + fld.id);
    }
    if (['radio', 'checkbox', 'dropdown', 'multiselect'].indexOf(fld.type) > -1 && !$.isArray(fld['choices'])) {
      throw new Error('Error in field ' + fld['id'] + ': choices property is missing or invalid');
    }
    if (fld.type === 'datetimepicker') {
      fld.options = $.extend({
        format: 'MM/DD/YYYY'
      }, fld.options);
    }
    if (fld.type === 'daterangepicker') {
      fld.options = $.extend({
        locale: {
          format: 'MM/DD/YYYY',
          separator: " - "
        }
      }, fld.options);
    }

  });
  return fields;
};
//class is a reserved keyword, it should never have been used, and is causing issues in IE now
//here we try fix any instances of usage of class instead of the newer className
comJS_form.fixClassProperty = function (objectOrArray) {
  $.each($.makeArray(objectOrArray || []), function (i, o) {
    if (o['className'] === undefined && typeof o['class'] === 'string') { //this is a hack
      o['className'] = o['class'];
      delete o['class'];
    }
  });
};
var comJS_section = function (o) {
  comJS_form.fixClassProperty(o);
  this.id = o.id;
  this.title = o.title;
  this.className = o['className'];
  this.rows = [];
};

var comJS_row = function (o) {
  this.fields = comJS_form.addDefaultFieldProperties(o); //this is an array of raw javascript objects describing fields
  this.type = 'standard';
};

var comJS_grid = function (o) {
  comJS_form.fixClassProperty(o);
  this.id = (o.id || "") ? o.id : 'grid-' + Math.floor(Math.random() * 100000000);
  this.add = (o.add || "") ? true : false;
  this.className = o['className'];
  this.title = o.title;
  this.headers = o.headers;
  this.fields = comJS_form.addDefaultFieldProperties(o.fields);
  this.type = 'grid';
};

comJS_form.prototype.addSection = function (o) {
  if (!(o instanceof comJS_section)) {
    o = new comJS_section(o);
  }
  this.sections.push(o);
  return o;
};

comJS_section.prototype.addRow = function (o) {
  if (!(o instanceof comJS_row)) {
    o = new comJS_row(o);
  }
  this.rows.push(o);
  return this;
};

comJS_section.prototype.addGrid = function (o) {
  if (!(o instanceof comJS_grid)) {
    o = new comJS_grid(o);
  }
  this.rows.push(o);
  return this;
};

comJS_form.prototype.render = function (o) {
  /*
   o = {
   target: '#element_id', //required. specify a css selector to where the form should be rendered
   formValidationSettings: {} //optional, when specified, the attributes in here are passed through to the formValidation constructor: http://formvalidation.io/settings/
   }
   */
  var app = this;
  var oVal = {
    fields: {}
  };
  var form = document.createElement('form');
  form.id = this.id;
  form.className = 'comJS-form';
  form.setAttribute("data-fv-framework", "bootstrap");
  form.setAttribute("data-fv-icon-valid", "glyphicon glyphicon-ok");
  form.setAttribute("data-fv-icon-invalid", "glyphicon glyphicon-remove");
  form.setAttribute("data-fv-icon-validating", "glyphicon glyphicon-refresh");

  if (this.title || "") {
    var formHead = form.appendChild(document.createElement('h2'));
    formHead.textContent = this.title;
  }
  $.each(this.sections, function (i, section) {
    var oPanel = form.appendChild(document.createElement('div'));
    oPanel.id = section.id;


    oPanel.className = (section['className'] !== undefined) ? 'panel ' + section.className : "panel panel-default";
    if (section.title || "") {
      var oPanelHead = oPanel.appendChild(document.createElement('div'));
      oPanelHead.className = 'panel-heading';
      var oH3 = oPanelHead.appendChild(document.createElement('h3'));
      var oSpan = oH3.appendChild(document.createElement('span'));
      oSpan.className = "glyphicon glyphicon-th-large";
      oH3.appendChild(document.createElement('span'));
      oH3.textContent = section.title;
    }
    var oPanelBody = oPanel.appendChild(document.createElement('div'));
    oPanelBody.className = 'panel-body';

    $.each(section.rows, function (k, row) {
      var oRow = oPanelBody.appendChild(document.createElement('div'));
      oRow.className = 'row';
      if (row.type == 'grid') {
        app.processGrid(oRow, oVal, row);
      } else {
        $.each(row.fields, function (l, field) {
          app.processField(oRow, oVal, row, field);
        });
      }
    });
  });
  $(o.target).append(form);
  $.each(this.sections, function (i, section) {
    $.each(section.rows, function (k, row) {
      app.initializePluginsInRow(row);
    });
  });


  //INITIATE FORM VALIDATION
  var frm = $('#' + this.id);
  var options = $.extend({
    excluded: [':not(.multiselect):disabled', ':not(.multiselect):hidden', ':not(.multiselect):not(:visible)'], //exclude all hidden and disabled fields that are not multiselects
    feedbackIcons: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    onSuccess: this.success,
    onError: function (e) {
      console.log('Validation error occurred:', e);
      var moveTo = $($(".has-error input, .has-error select, .has-error button, .has-error textarea")[0]);
      moveTo.focus();
      if (moveTo[0]['getBoundingClientRect']) {
        var rect = moveTo[0].getBoundingClientRect();
        if (rect.top < 0 || rect.top > $(window).height() - 50) {
          $('html,body').animate({
            scrollTop: Math.max(0, moveTo.offset().top - 100)
          }, 'slow');
        }
      }
    },
    fields: oVal.fields
  }, o['formValidationSettings'] || {});

  frm.formValidation(options)
    .on('err.field.fv', function (e) {
      $(e.target).closest('.form-group').find('input,select,textarea').attr('aria-invalid', true);
    })
    .on('success.field.fv', function (e, data) {
      $(e.target).closest('.form-group').find('input,select,textarea').attr('aria-invalid', false);
    });
  frm.find("button.fv-hidden-submit").text("hidden submit button");
  frm.find("button.fv-hidden-submit").attr("aria-hidden", true);

  app.fixFormValidationRender(frm);
};

comJS_form.prototype.fixFormValidationRender = function (el) {
  el.find('i.form-control-feedback').attr('aria-hidden', 'true');
  //this will override feedback icon insertion into a wrong place after the form is rendered
  el.find("label.radioLabel>i, label.checkboxLabel>i").each(function () {
    var $this = $(this);
    $this.insertAfter($this.closest('fieldset').find('legend'));
  });
  el.find('div.datetimepicker').each(function () {
    //because the datetimepicker div.entryField also has .input-group class, the feedback icons are put in the wrong spot
    var $this = $(this);
    $this.parent().find('i.form-control-feedback').insertAfter($this.find('input'));
  });
};

comJS_form.prototype.initializePluginsInRow = function (row, $container) {
  if (!$container) {
    $container = $('#' + this.id);
  }
  var that = this;
  $.each(row.fields, function (l, field) {
    switch (field['type']) {
      case 'multiselect':
        var $el = $container.find("." + field.id + ".multiselect");
        (window['comJS_app'] || window['ComJSApp']).addMultiselect({
          $select: $el,
          ariaLabelledBy: $el.attr('aria-labelledby'),
          ariaDescribedBy: $el.attr('aria-describedby'),
          ariaRequired: $el.attr('aria-required'),
          multiselectOptions: field.options
        });

        break;
      case 'daterangepicker':
        $container.find(".daterangevalidation") //TODO: support multiple instances of daterangepicker
          .daterangepicker(field.options)
          .on('show.daterangepicker', function (ev, picker) {
            $($(picker)[0]).focus();
          });
        $('.daterangepicker').attr('aria-hidden', true);
        break;
      case 'datetimepicker':
        $container.find("." + field.id + ".datetimepicker")
          .datetimepicker(field.options)
          .on("dp.change", function () {
            var sName = $(this).attr("data-refid");
            $("#" + that.id).data('formValidation').updateStatus(sName, 'NOT_VALIDATED').validateField(sName);
          });
        break;
      default:
        if (field.validationtype === "Phone" && IEversion >= 10) {
          if (typeof that['rootPath'] !== 'string') {
            throw new Error('rootPath must be defined for comJS form when using Phone fields');
          }
          $container.find("#" + field.id + ".phonevalidation, [name$=\"." + field.id + "\"].phonevalidation")
            .each(function (i, el) {
              var $el = $(el);
              if ($el.attr('name').indexOf('[template]') === -1) {
                $el.intlTelInput($.extend({
                  autoPlaceholder: false,
                  autoFormat: true,
                  preferredCountries: ['ca']
                }, field.options || {}, {
                  utilsScript: that.rootPath + 'js/utils.js'
                }));
                $el.on("country-change", function () {
                  var sName = $(this).attr("name");
                  $("#" + that.id).data('formValidation').updateStatus(sName, 'NOT_VALIDATED').validateField(sName);
                });
                $el.on("keyup change", function (evt) {
                  var input = $(evt.currentTarget);
                  if (typeof intlTelInputUtils !== 'undefined') { // utils are lazy loaded, so must check
                    var currentText = input.intlTelInput("getNumber", intlTelInputUtils.numberFormat.E164);
                    if (typeof currentText === 'string') { // sometimes the currentText is an object :)
                      input.intlTelInput('setNumber', currentText); // will autoformat because of formatOnDisplay=true
                    }
                  }
                });
              }
            });

          $container.find('.flag-dropdown').attr('aria-hidden', true);

        }
    }

  });
  $container.find('[data-toggle="tooltip"]').tooltip({
    "html": true
  });
};

comJS_form.prototype.processGrid = function (oRow, oVal, row) {
  var app = this,
    oBTN;
  var oGrid = oRow.appendChild(document.createElement('div'));
  oGrid.id = row.id;
  oGrid.className = 'grid-object table-responsive ';
  oGrid.className += row['className'] || '';
  oGrid.className += (row.addclass || '') ? " " + row.addclass : '';
  app[oGrid.id + "-index"] = 0;
  var oGridHead = oGrid.appendChild(document.createElement('h4'));
  oGridHead.className = 'grid-title';
  oGridHead.textContent = row.title;
  var oTable = oGrid.appendChild(document.createElement('table'));
  oTable.className = 'grid-table table table-striped';
  var oTR = oTable.appendChild(document.createElement('tr'));

  //ADD HEADERS
  $.each(row.headers, function (i, header) {
    var oTH = oTR.appendChild(document.createElement('th'));
    oTH.textContent = header.title;
    oTH.id = row.id + "_header_" + i;
  });
  //ADD AN EXTRA COLUMN WHICH WILL BE USED TO HOLD THE ADD/DELETE BUTTONS
  oTH = oTR.appendChild(document.createElement('th'));
  var oSpan = oTH.appendChild(document.createElement('span'));
  oSpan.className = "sr-only";
  oSpan.textContent = "Add/Remove Rows";

  //ADD FIRST ROW OF GRID
  oTR = oTable.appendChild(document.createElement('tr'));
  oTR.id = row.id + "-row-0";
  oTR.setAttribute('data-row-index', "0");
  var fieldDefinitions = {}; //used to get options when adding new rows dynamically
  $.each(row.fields, function (l, field) {
    var oFieldDiv = oTR.appendChild(document.createElement('td'));
    oFieldDiv.className = "form-group";
    oFieldDiv.className += (field.addclass || '') ? " " + field.addclass : '';
    field.grid = "0";
    field.gridlabel = row.id + "_header_" + l;
    if (l === 0) {
      var span = oFieldDiv.appendChild(document.createElement('span'));
      span.className = 'sr-only';
      span.id = 'row_sr_label_0';
      span.textContent = row.title + ' row 1';
    }
    app.addformfield(field, oFieldDiv);
    //create a validator specifically for the zero row.
    var tmpfieldId = field.id;
    field.id = "row[0]." + field.id;
    app.addfieldvalidation(oVal, field, oFieldDiv);
    field.id = tmpfieldId;
    fieldDefinitions[field.id] = field;
  });

  //ADD A FAKE REMOVE BUTTON AT THE END OF THE FIRST ROW
  var oTD = oTR.appendChild(document.createElement('td'));
  oTD.className = 'text-right';
  oBTN = oTD.appendChild(document.createElement('button'));
  oBTN.className = 'btn btn-default grid-minus';
  oBTN.type = 'button';
  oBTN.disabled = true;
  oBTN.title = 'Remove this row from ' + row.title;
  oBTN.appendChild(document.createElement('span')).className = 'glyphicon glyphicon-minus';
  var oSpan = oBTN.appendChild(document.createElement('span'));
  oSpan.className = 'sr-only';
  oSpan.textContent = "Remove Row";

  //ADD GRID TEMPLATE THAT CAN BE USED TO CREATE NEW ROWS
  oTR = oTable.appendChild(document.createElement('tr'));
  oTR.id = oGrid.id + "-template";
  oTR.className = "hide";
  $.each(row.fields, function (l, field) {
    var oFieldDiv = oTR.appendChild(document.createElement('td'));
    oFieldDiv.className = "form-group";
    oFieldDiv.className += field.addclass ? " " + field.addclass : '';
    field.grid = "template";
    if (l === 0) {
      var span = oFieldDiv.appendChild(document.createElement('span'));
      span.className = 'sr-only';
      span.id = 'row_sr_label_template';
      span.textContent = row.title + ', row template';
    }
    app.addformfield(field, oFieldDiv);
  });

  //ADD A BUTTON AT THE END OF THE TEMPLATE ROW TO REMOVE A ROW FROM THE GRID
  oTD = oTR.appendChild(document.createElement('td'));
  oTD.className = 'text-right';
  oBTN = oTD.appendChild(document.createElement('button'));
  oBTN.type = 'button';
  oBTN.className = 'btn btn-default grid-minus';
  oBTN.title = 'Remove this row from ' + row.title;
  oSpan = oBTN.appendChild(document.createElement('span'));
  oSpan.className = 'glyphicon glyphicon-minus';
  oSpan = oSpan.appendChild(document.createElement('span'));
  oSpan.className = 'sr-only';
  oSpan.textContent = 'Remove Row';

  //Add a 'new' button to the last row
  oTR = oTable.appendChild(document.createElement('tr'));
  oTD = oTR.appendChild(document.createElement('td'));
  oTD.colSpan = row.fields.length + 1;
  oBTN = oTD.appendChild(document.createElement('button'));
  oBTN.className = 'btn btn-default pull-right grid-add';
  oBTN.type = 'button';
  oBTN.onclick = function () {
    app[oGrid.id + "-index"]++;
    var rowIndex = app[oGrid.id + "-index"];
    //CLONE THE TEMPLATE TO CREATE A NEW GRID ROW
    var $template = $('#' + oGrid.id + '-template');
    var $clone = $template
      .clone()
      .removeClass('hide')
      .attr('id', oGrid.id + '-row-' + rowIndex)
      .attr('data-row-index', rowIndex);
    var html = $clone.html();
    html = html.replace(/, row template/g, ', row ' + (parseInt(rowIndex) + 1));
    html = html.replace(/template/g, rowIndex);
    $clone.html(html);
    $clone.insertBefore($template);

    //ADD THE PROPER DELETE FUNCTION TO THE DELETE ROW BUTTON FOR THE NEW ROW
    $clone.find('.grid-minus').click(function () {
      var $row = $(this).closest('tr');
      $.each($row.find('.form-control'), function (i, item) {
        var $item = $(item);
        var itemId = item.tagName.toUpperCase() === 'FIELDSET' ? $item.find('input').first().attr('name') : $item.attr('name');
        $('#' + app.id).formValidation('removeField', itemId);
      });
      var focusEl = $row.prev().find('input,select,textarea').first();
      $row.remove();
      focusEl.focus();
    });

    //ADD EACH FIELD IN THE NEW GRID ROW TO THE FORM VALIDATOR
    var arrNewFields = $clone.find('.form-control');
    $.each(arrNewFields, function (i, item) {
      var $item = $(item);
      var itemId = item.tagName.toUpperCase() === 'FIELDSET' ? $item.find('input').first().attr('name') : $item.attr('name'); //this looks like rows[x].name
      var definition = fieldDefinitions[itemId.split('.')[1]];
      var validatorOptions = app.validatorOptions(definition);
      app.addValidatorMessageDiv(definition, validatorOptions, $item.closest('td')[0], rowIndex);
      $('#' + app.id).formValidation('addField', itemId, validatorOptions);
    });
    app.initializePluginsInRow(row, $clone);
    app.fixFormValidationRender($clone);
    $clone.find('input,select,textarea').first().focus();
  };
  oBTN.appendChild(document.createElement('span')).className = 'glyphicon glyphicon-plus';
  var oSpan = oBTN.appendChild(document.createElement('span'));
  oSpan.textContent = 'Add Row to ' + row.title;
};

comJS_form.prototype.processField = function (oRow, oVal, row, field) {
  var intFields = row.fields.length;
  var oField = oRow.appendChild(document.createElement('div'));
  oField.id = field.id + 'Element';
  oField.className = field['className'] || ((intFields == 1) ? "col-xs-12" : (intFields == 2) ? "col-xs-12 col-sm-6" : (intFields == 3) ? "col-xs-12 col-md-4" : "col-xs-12 col-sm-6 col-md-3");
  oField.className += ' form-group form-group-';
  oField.className += field.orientation || 'vertical';
  oField.className += field.addclass ? " " + field.addclass : '';
  var oFieldDiv = oField.appendChild(document.createElement('div'));

  //LABEL
  if (['html', 'button'].indexOf(field.type) == -1) {
    var useLabel = ['static', 'checkbox', 'radio'].indexOf(field.type) === -1;
    if (useLabel || field.title) {
      var label = oFieldDiv.appendChild(document.createElement(useLabel ? 'label' : 'span'));
      label.className = useLabel ? 'control-label' : 'staticlabel' + (field.type != 'static' ? ' ' + field.type : '');
      if (useLabel) {
        label.htmlFor = field.id;
      }
      var titleSpan = label.appendChild(document.createElement('span'));
      titleSpan.textContent = field.title;
      if (!field.required && field.type != 'static') {
        var optionalLabel = label.appendChild(document.createElement('span'));
        optionalLabel.className = 'optional';
        optionalLabel.textContent = '(optional)';
      }
      if (field.infohelp) {
        var tooltip = label.appendChild(document.createElement('span'));
        tooltip.className = 'glyphicon glyphicon-info-sign';
        tooltip.setAttribute('data-toggle', 'tooltip');
        tooltip.setAttribute('data-placement', 'top');
        tooltip.tabIndex = 0;
        tooltip.title = field.infohelp;
      }
    }
  }
  this.addprehelptext(field, oFieldDiv);
  this.addformfield(field, oFieldDiv);
  this.addposthelptext(field, oFieldDiv);
  this.addfieldvalidation(oVal, field, oFieldDiv);

};

comJS_form.prototype.addprehelptext = function (fieldDefinition, fieldContainer) {
  if (fieldDefinition['prehelptext']) {
    var oHelp = fieldContainer.appendChild(document.createElement('p'));
    oHelp.className = 'helptext';
    oHelp.id = 'prehelptext_' + fieldDefinition.id;
    oHelp.innerHTML = fieldDefinition.prehelptext;
  }
};
comJS_form.prototype.addformfield = function (fieldDefinition, fieldContainer) {
  fieldContainer.appendChild(this.callFunction(this[fieldDefinition.type + 'FieldRender'], fieldDefinition, fieldContainer));
  if (fieldDefinition['prehelptext']) {
    //this only works after the field is in the DOM
    this.updateDescribedBy(fieldContainer, 'prehelptext_' + fieldDefinition.id);
  }
};

comJS_form.prototype.addposthelptext = function (fieldDefinition, fieldContainer) {
  if (fieldDefinition['posthelptext']) {
    var oHelp = fieldContainer.appendChild(document.createElement('p'));
    oHelp.className = 'helptext';
    oHelp.id = 'posthelptext_' + fieldDefinition.id;
    oHelp.innerHTML = fieldDefinition.posthelptext;
    this.updateDescribedBy(fieldContainer, oHelp.id);
  }
};

comJS_form.prototype.validatorOptions = function (fieldDefinition) {
  var validators = {};

  if (fieldDefinition.required) {
    validators.notEmpty = {
      message: fieldDefinition['requiredMessage'] || (fieldDefinition.title + ' is required and cannot be left blank')
    };
  }

  if (fieldDefinition.type === "datetimepicker") {
    validators.callback = {
      message: fieldDefinition['validationMessage'] || ('The date must be in the format ' + fieldDefinition.options.format),
      callback: function (value) {
        return (value === '' && !fieldDefinition.required) || moment(value, fieldDefinition.options.format, true).isValid();
      }
    };
  } else if (fieldDefinition.type === "daterangepicker") {
    validators.callback = {
      message: fieldDefinition['validationMessage'] || ('The dates must be in the format ' + fieldDefinition.options.locale.format + fieldDefinition.options.locale.separator + fieldDefinition.options.locale.format),
      callback: function (value) {
        var dates = value.split(fieldDefinition.options.locale.separator);
        return (value === '' && !fieldDefinition.required) ||
          (dates.length === 2 &&
            moment(dates[0], fieldDefinition.options.locale.format, true).isValid() &&
            moment(dates[1], fieldDefinition.options.locale.format, true).isValid());
      }
    };
  } else {
    switch (fieldDefinition.validationtype) {
      case 'Phone':
        validators.callback = {
          message: fieldDefinition['validationMessage'] || 'This field must be a valid phone number.',
          callback: function (value, validator, $field) {
            if (IEversion < 10) {
              if (fieldDefinition.required || value !== "") {
                if (value.match(/\d{3}-?\d{3}-?\d{4}/) && value.match(/\d{3}-?\d{3}-?\d{4}/)[0] == value) {
                  $field.val(value.replace(/(\d{3})\-?(\d{3})\-?(\d{4})/, '$1-$2-$3'));
                  return true;
                } else {
                  return false;
                }
              } else {
                return true;
              }
            } else {
              return value === '' || $field.intlTelInput('isValidNumber');
            }
          }
        };
        break;
      case 'Email':
        validators.emailAddress = {
          message: fieldDefinition['validationMessage'] || 'The value is not a valid email address'
        };
        break;
      case 'URL':
        validators.uri = {
          message: fieldDefinition['validationMessage'] || 'The value is not a valid URL (http://xx.xx or https://xx.xx).'
        };
        break;
      case 'PostalCode':
        validators.regexp = {
          regexp: /^(?!.*[DFIOQU])[A-VXY][0-9][A-Z] ?[0-9][A-Z][0-9]$/i,
          message: fieldDefinition['validationMessage'] || 'This field must be a valid postal code'
        };
        break;
    }
  }
  var retVal = {
    validators: $.extend(validators, fieldDefinition['validators'] || {})
  };
  if (fieldDefinition.type == 'dropzone') {
    retVal.excluded = false;
  }
  return retVal;
};

comJS_form.prototype.addfieldvalidation = function (formValidatorFields, fieldDefinition, fieldContainer) {
  //ADD VALIDATION
  var validatorOptions = this.validatorOptions(fieldDefinition);
  this.addValidatorMessageDiv(fieldDefinition, validatorOptions, fieldContainer, fieldDefinition['grid']);
  formValidatorFields.fields[fieldDefinition.id] = validatorOptions;
};

comJS_form.prototype.addValidatorMessageDiv = function (fieldDefinition, validatorOptions, fieldContainer, gridRowIndex) {
  if (!$.isEmptyObject(validatorOptions.validators)) {
    var errorMessageDiv = fieldContainer.appendChild(document.createElement('div'));
    errorMessageDiv.id = 'fv_err_msg_' + Math.random().toString().split('.')[1] + '_' + (gridRowIndex || '');
    errorMessageDiv.className = 'fv-err-msg';
    this.updateDescribedBy(fieldContainer, errorMessageDiv.id);
    validatorOptions['err'] = '#' + errorMessageDiv.id;
  }
};

comJS_form.prototype.updateDescribedBy = function (targetFieldContainer, fieldDescribedByElementId) {
  var $fields = $(targetFieldContainer).find('input,textarea,select');
  var currentValues = $fields.attr('aria-describedby') ? $fields.attr('aria-describedby').split(' ') : [];
  $fields.attr('aria-describedby', currentValues.concat([fieldDescribedByElementId]).join(' '));
};

comJS_form.prototype.callFunction = function (func) {
  var ret = func.apply(this, Array.prototype.slice.call(arguments, 1));
  return ret;
};

comJS_form.prototype.staticFieldRender = function (field, oLabel) {
  var o = document.createElement('p');
  o.name = (field.grid || "") ? "row[0]." + field.id : field.id;
  o.textContent = field.value;
  return o;
};

comJS_form.prototype.htmlFieldRender = function (field, oLabel) {
  var o = document.createElement('div');
  o.name = (field.grid || "") ? "row[0]." + field.id : field.id;
  o.innerHTML = field.html;
  return o;
};

comJS_form.prototype.textFieldRender = function (field, oLabel, typeOverride) {
  var o = oLabel.appendChild(document.createElement('div'));
  o.className = 'entryField';
  var oField = o.appendChild(document.createElement('input'));
  if (field['htmlAttr']) {
    $(oField).attr(field['htmlAttr']);
  }
  oField.title = field.title;
  oField.type = typeOverride || 'text';
  oField.value = (field.value || "") ? field.value : '';
  oField.disabled = (field.disabled || "") ? "disabled" : false;
  if (field.grid || "") {
    oField.name = "row[" + field.grid + "]." + field.id;
    $(oField).attr("aria-labelledby", 'row_sr_label_' + field.grid + ' ' + field.gridlabel);
  } else {
    oField.name = field.id;
    oField.id = field.id;
  }
  //SET THE REQUIRED FIELD DECLARATVE FORM VALIDATION ATTRIBUTES
  if (field.required) {
    oField.setAttribute("aria-required", "true");
    oField.className = 'form-control required';
  } else {
    oField.className = 'form-control';
  }

  if (field.validationtype == "Phone") {
    oField.className += " phonevalidation ";
  }
  oField.placeholder = (field.placeholder || "") ? field.placeholder : "";

  return o;
};

comJS_form.prototype.passwordFieldRender = function (field, oLabel) {
  return this.textFieldRender(field, oLabel, 'password');
};

comJS_form.prototype.radioFieldRender = function (field) {
  var o = document.createElement('fieldset');
  o.className = 'form-control';
  var oLegend = o.appendChild(document.createElement('legend'));
  oLegend.className = "sr-only";
  oLegend.textContent = "Select an option for " + (field.title || field.id);

  $.each(field.choices, function (m, choice) {
    var oDiv = o.appendChild(document.createElement('label'));
    oDiv.className = (field.orientation || '') ? field.orientation : 'vertical';
    oDiv.className += ' entryField radioLabel';
    var oField = oDiv.appendChild(document.createElement('input'));
    if (field.grid || "") {
      oField.name = "row[" + field.grid + "]." + field.id;
    } else {
      oField.name = field.id;
      oField.id = field.id + '_' + m;
    }
    if (field['required']) {
      oField.setAttribute('aria-required', 'true');
    }
    oField.type = 'radio';
    oField.className = (field.required || "") ? 'required' : '';
    oField.value = choice.hasOwnProperty('value') ? choice.value : choice.text;
    oField.disabled = (field.disabled || "") ? "disabled" : false;
    if (field.value || "") {
      oField.checked = (field.value == oField.value) ? 'checked' : '';
    }
    oDiv.appendChild(document.createElement('span')).innerHTML = choice.text;
  });

  return o;
};

comJS_form.prototype.checkboxFieldRender = function (field) {
  var o = document.createElement('fieldset');
  o.className = 'form-control';
  var oLegend = o.appendChild(document.createElement('legend'));
  oLegend.className = "sr-only";
  oLegend.textContent = "Select options for " + (field.title || field.id);

  $.each(field.choices, function (m, choice) {
    var oDiv = o.appendChild(document.createElement('label'));
    oDiv.className = (field.orientation || '') ? field.orientation : 'vertical';
    oDiv.className += ' entryField checkboxLabel';
    var oField = oDiv.appendChild(document.createElement('input'));
    if (field.grid || "") {
      oField.name = "row[" + field.grid + "]." + field.id;
    } else {
      oField.name = field.id;
      oField.id = field.id + '_' + m;
    }
    if (field['required']) {
      oField.setAttribute('aria-required', 'true');
    }
    oField.type = 'checkbox';
    oField.className = (field.required || "") ? 'required' : '';
    oField.value = choice.hasOwnProperty('value') ? choice.value : choice.text;
    oField.disabled = (field.disabled || "") ? "disabled" : false;
    if (choice.selected || "") {
      oField.checked = "checked";
    }
    oDiv.appendChild(document.createElement('span')).innerHTML = choice.text;
  });

  return o;
};

comJS_form.prototype.dropdownFieldRender = function (field) {
  var o = document.createElement('div');
  o.className = 'entryField dropdown-entry-field';
  var oField = o.appendChild(document.createElement('select'));
  if (field.required) {
    oField.setAttribute("aria-required", "true");
  }
  if (field.grid || "") {
    oField.name = "row[" + field.grid + "]." + field.id;
    $(oField).attr("aria-labelledby", 'row_sr_label_' + field.grid + ' ' + field.gridlabel);
  } else {
    oField.name = field.id;
    oField.id = field.id;
  }
  oField.className = 'form-control';
  $.each(field.choices, function (m, choice) {
    var oOption = oField.appendChild(document.createElement('option'));
    oOption.value = choice.hasOwnProperty('value') ? choice.value : choice.text;
    oOption.text = choice.text;
    if (field.value || "") {
      oOption.selected = (field.value == oOption.value) ? 'selected' : '';
    }
  });
  oField.disabled = (field.disabled || "") ? "disabled" : false;
  return o;
};

comJS_form.prototype.multiselectFieldRender = function (field) {
  var o = document.createElement('div');
  o.className = 'entryField';
  var oField = o.appendChild(document.createElement('select'));
  if (field.required) {
    oField.setAttribute("aria-required", "true");
  }
  if (field.grid || "") {
    oField.name = "row[" + field.grid + "]." + field.id;
    $(oField).attr("aria-labelledby", 'row_sr_label_' + field.grid + ' ' + field.gridlabel);
  } else {
    oField.name = field.id;
    oField.id = field.id;
  }
  oField.style.display = 'none';
  oField.setAttribute('aria-hidden', true);
  oField.className = 'form-control multiselect ' + field.id;
  oField.multiple = field.multiple ? 'multiple' : '';
  $.each(field.choices, function (m, choice) {
    var oOption = oField.appendChild(document.createElement('option'));
    oOption.value = choice.hasOwnProperty('value') ? choice.value : choice.text;
    oOption.text = choice.text;
  });
  oField.disabled = (field.disabled || "") ? "disabled" : false;
  return o;
};

comJS_form.prototype.daterangepickerFieldRender = function (field) {
  var o = document.createElement('div');
  o.className = 'entryField';
  var oField = o.appendChild(document.createElement('input'));
  if (field.grid || "") {
    oField.name = "row[" + field.grid + "]." + field.id;
    $(oField).attr("aria-labelledby", 'row_sr_label_' + field.grid + ' ' + field.gridlabel);
  } else {
    oField.name = field.id;
    oField.id = field.id;
  }
  oField.type = 'text';
  oField.value = (field.value || "") ? field.value : '';
  oField.className = (field.required || "") ? 'form-control required daterangevalidation' : 'form-control daterangevalidation';
  if (field.required) {
    oField.setAttribute("aria-required", "true");
  }
  oField.disabled = (field.disabled || "") ? "disabled" : false;
  oField.placeholder = field.placeholder || "";
  return o;
};

comJS_form.prototype.datetimepickerFieldRender = function (field) {
  var o = document.createElement('div');
  o.className = 'input-group date entryField datetimepicker ' + field.id;
  o.setAttribute("data-refid", field.id);
  var oField = o.appendChild(document.createElement('input'));
  oField.type = 'text';
  if (field['required']) {
    oField.setAttribute("aria-required", "true");
    oField.className = 'form-control required';
  } else {
    oField.className = 'form-control';
  }
  oField.value = field['value'] || '';
  if (field['grid']) {
    oField.name = "row[" + field.grid + "]." + field.id;
    $(oField).attr("aria-labelledby", 'row_sr_label_' + field.grid + ' ' + field.gridlabel);
  } else {
    oField.name = field.id;
    oField.id = field.id;
  }
  oField.className = 'form-control';
  var oSpan = o.appendChild(document.createElement('span'));
  oSpan.className = 'input-group-addon';
  oSpan.setAttribute('aria-hidden', true);
  oSpan = oSpan.appendChild(document.createElement('span'));
  oSpan.className = 'glyphicon ' + (field['glyphicon'] || 'glyphicon-calendar');

  oField.disabled = field['disabled'] ? "disabled" : false;
  oField.placeholder = field.placeholder || "";
  return o;
};

comJS_form.prototype.textareaFieldRender = function (field) {
  var o = document.createElement('div');
  o.className = 'entryField';
  var oField = o.appendChild(document.createElement('textarea'));
  if (field['htmlAttr']) {
    $(oField).attr(field['htmlAttr']);
  }
  if (field.grid || "") {
    oField.name = "row[" + field.grid + "]." + field.id;
    $(oField).attr("aria-labelledby", 'row_sr_label_' + field.grid + ' ' + field.gridlabel);
  } else {
    oField.name = field.id;
    oField.id = field.id;
  }
  if (field.cols) {
    oField.cols = field.cols;
  }
  if (field.rows) {
    oField.rows = field.rows;
  }
  oField.title = field.title;
  oField.type = 'text';
  oField.className += (field.required || "") ? 'form-control required' : 'form-control';
  if (field.required) {
    oField.setAttribute("aria-required", "true");
  }
  oField.placeholder = field.placeholder || "";
  oField.value = (field.value || "") ? field.value : '';
  oField.disabled = (field.disabled || "") ? "disabled" : false;
  return o;
};

comJS_form.prototype.buttonFieldRender = function (field) {
  var o = document.createElement('button');
  o.type = 'button';
  if (field['className'] && !field['btnClass']) {
    //field['className'] should probably never have been applied here,
    //but to avoid a breaking change, we don't apply field['className'] if the newer field['btnClass'] is used
    o.className = field.className;
  } else {
    o.className = 'btn btn-' + (field['btnClass'] || 'success');
  }
  var oSpan = o.appendChild(document.createElement('span'));
  oSpan.className = (field.glyphicon || "") ? 'glyphicon ' + field.glyphicon : '';
  oSpan = o.appendChild(document.createElement('span'));
  oSpan.textContent = field.title;
  o.disabled = (field.disabled || "") ? "disabled" : false;
  $(o).on('click', field['onclick'] || function () {
  });
  return o;
};

comJS_form.prototype.dropzoneFieldRender = function (field) {

  // Main element.
  var $el = $('<div></div>');

  // Hidden input element.
  // For form.getData() and 'required' validation.
  var $hiddenInput = $('<input type="hidden">')
    .attr('data-fv-field', field.id)
    .attr('id', field.id)
    .attr('name', field.id);
  if (field.required) {
    $hiddenInput
      .attr('aria-required', 'true')
      .attr('class', 'required');
  }
  $el.append($hiddenInput);

  // Dropzone div element.
  var $dropzoneDiv = $('<div class="dropzone"></div>');
  $el.append($dropzoneDiv);

  // Dropzone.
  var comJSDropzone = $hiddenInput.get(0).comJSDropzone = new comJSDropzone();

  // Fill from model.
  comJSDropzone._fillFromModel = function (model) {
    if (field.bindTo) {

      // Get files as array.
      var files = model.get(field.bindTo) || [];
      if (typeof files === 'string') {
        try {
          files = JSON.parse(files);
          if (!Array.isArray(files)) {
            files = [];
          }
        } catch (e) {
          files = [];
        }
      }
      if (!Array.isArray(files)) {
        files = [files];
      }

      // Set initial files.
      comJSDropzone.initFiles = files.map(function (file) {
        file.status = 'initial';
        return file;
      });

      // Reset dropzone files.
      comJSDropzone.dropzone.removeAllFiles(true);
      for (var i = 0; i < files.length; i++) {
        var file = files[i];
        comJSDropzone.dropzone.emit('addedfile', file);
        comJSDropzone.dropzone.emit('complete', file);
        comJSDropzone.dropzone.files.push(file);
      }

      // Update hidden input.
      comJSDropzone._updateHiddenIntput();
    }
  };

  // Clone options.
  var options = $.extend(true, {}, field.options);

  // Update hidden input.
  if (!options.valueMap) {
    options.valueMap = function (file) {
      var bin_id;
      try {
        bin_id = JSON.parse(file.xhr.response).BIN_ID[0];
      } catch (e) {
        bin_id = null;
      }
      return {
        bin_id: bin_id,
        name: file.name,
        size: file.size,
        status: file.status,
        type: file.type
      }
    }
  }
  comJSDropzone._updateHiddenIntput = function () {
    var value = comJSDropzone.dropzone.files.filter(function (file) {
      return file.status == 'initial' || file.status == 'success'
    }).map(options.valueMap);
    var textValue = value.length > 0 ? JSON.stringify(value) : '';
    if (textValue != $hiddenInput.val()) {
      $hiddenInput.val(textValue).trigger('change');
      $hiddenInput.closest('form').data('formValidation').revalidateField($hiddenInput);
    }
  };
  comJSDropzone._watchChanges = function (form) {
    if (field.bindTo) {
      $hiddenInput.on('change', function (e) {
        if (form._model) {
          var newValue = comJSDropzone.dropzone.files.filter(function (file) {
            return file.status == 'initial' || file.status == 'success'
          });
          form._model.set(field['bindTo'], newValue);
        }
      });
    }
  };
  comJSDropzone.finalize = function (cbk) {
    var step2 = function () {
      if (!comJSDropzone.initFiles) {
        comJSDropzone.initFiles = [];
      }
      var deletable = comJSDropzone.initFiles.filter(function (file) {
        return comJSDropzone.dropzone.files.indexOf(file) == -1;
      });
      var keepable = comJSDropzone.dropzone.files.filter(function (file) {
        return comJSDropzone.initFiles.indexOf(file) == -1;
      });
      if (cbk) {
        cbk({
          delete: deletable.filter(function (file) {
            return file.status == 'initial' || file.status == 'success';
          }),
          keep: keepable.filter(function (file) {
            return file.status == 'initial' || file.status == 'success';
          })
        });
      }
    };
    var step1 = function () {
      if (comJSDropzone.dropzone.getQueuedFiles().length > 0) {
        var success = function () {
          comJSDropzone.dropzone.off('success', success);
          step1();
        };
        comJSDropzone.dropzone.on('success', success);
        comJSDropzone.dropzone.processQueue();
      } else {
        step2();
      }
    };
    step1();
  };

  // Dropzone options.
  options.selector = $dropzoneDiv;
  if (options.includecomJSFormInit != false) {
    options.init = (function (oldInit) {
      return function () {
        if (oldInit) {
          oldInit.apply(this, arguments);
        }
        this.on('success', function (file) {
          comJSDropzone._updateHiddenIntput();
        });
        this.on('removedfile', function (file) {
          comJSDropzone._updateHiddenIntput();
        });
      }
    })(options.init);
  }
  if (!options.clickable && options.includecomJSFormButton != false) {
    $el.append('<p><button type="button" class="btn btn-default" id="' + field.id + 'Btn">Upload File</button></p>');
    options.clickable = [$dropzoneDiv.get(0), $el.find('#' + field.id + 'Btn').get(0)];
  }

  // Render dropzone.
  comJSDropzone.render(options);

  // Return wrapper element.
  return $el.get(0);
};

/*
 comJSForm is a class to supercede and wrap around comJS_form.
 Example usage:
 var f = new comJSForm(def); //see below about the def argument
 var app = new comJS_app('my app');
 app.addForm(f, 'bottom');

 definition: a complete raw javascript object that defines a comJS_form. ex:
 {
   //these first four are the same as the properties passed to new comJS_form()
   id: 'my_form_id',
   title: 'My Form',
   rootPath: '/resources/my_app/', //only required when using any validationtype=Phone fields
   success: someFunctionDefinedSomewhereElse,

   useBinding: true, //defaults to false, set to true to use data binding with a ComJSModel object.
   //use in conjunction with the setModel method of comJSForm and the bindTo attribute of field definitions

   sections: [ //an array of sections on the form
     {
       //these first three are the same as the properties passed to new comJS_section()
       id: 'section_one',
       title: 'Section One',
       className: 'Some special class'

       rows: [ //an array of rows within the current section
         { //for each row, specify a grid OR an array of fields:
           fields: [ //an array of fields within the current row
             {
               type: '', //optional, enum: ['html', 'button', 'static', 'checkbox', 'radio', 'text' (default), 'daterangepicker', 'dropdown', 'multiselect', 'datetimepicker', 'textarea', 'password'], the type of field to add to the row
               id: 'field_one', //required, used to create the dom element id
               title: '', //required except for type=html|button|static, the title/label for the field
               className: 'col-xs-6', //optional, override the auto-generated css grid col classes, ex: col-xs-12
               //NOTE: if type=button, className is applied to button as well. if you DO NOT want this behaviour, you can explicitly specify the btnClass option below
               btnClass: 'success', //optional, only applies when type=button, defaults to 'success', determines the bootstrap btn-x class used to style the button, valid values are here: http://getbootstrap.com/css/#buttons-options
               orientation: 'horizontal', //optional, enum: ['horizontal','vertical']. default is vertical. this affects fields like radio
               addclass: 'additional-class', //optional, append to the auto-generated classes
               required: false, //optional, defaults to false
               requiredMessage: '', //optional, if required is set to true, this is used as the empty error message (instead of the default)
               infohelp: '', //optional, help text for the field, which is shown via a tooltip for an info icon, does not apply to type=html||button
               prehelptext: '', //optional, help text for the field which is always displayed, in front of the field
               posthelptext: '', //optional, help text for the field which is always displayed, after the field
               validators: {}, //optional, a validator object. see: http://formvalidation.io/validators/, ex: validators: {creditCard: {message: 'Invalid cc value'}}
                                //when required is true or validationtype is used or type is set to daterangepicker||datetimepicker, validators are auto-generated for you,
                                //but any validators that you specify here will override the auto-generated ones
               validationtype: 'Phone', //optional, enum: ['Phone', 'Email', 'URL','PostalCode'], if specified, this will automatically set the proper validators object
               validationMessage: '', //optional, when validationtype is used or type is set to daterangepicker||datetimepicker, this can be specified to override the default error message
               options: {}, //optional, a raw javascript object,
                //when type=daterangepicker||multiselect||datetimepicker OR validationtype=Phone, this is passed into the jquery constructor for the field
                //see http://davidstutz.github.io/bootstrap-multiselect/
                //see http://www.daterangepicker.com/
                //see http://eonasdan.github.io/bootstrap-datetimepicker/
                //see https://github.com/jackocnr/intl-tel-input/tree/v7.1.0#options
               value: '', //optional, the value or content of this field
               html: '', //optional, the html content, only applies when type=html
               disabled: false, //optional, defaults to false, only applies to fields that can be disabled
               placeholder: '', //optional, a placeholder string for input fields, doesn't apply if validationtype=Phone
               choices: [{text: '', value: ''}], //required when type=radio||checkbox||dropdown||multiselect, an array of text/value pairs, text is required, but value is not (defaults to text)
               multiple: false, //optional, defaults to false, only applies when type=multiselect, determines if multiple selection is allowed
               cols: '50', //optional, when type=textarea this specifies the cols attribute
               rows: '10', //optional, when type=textarea this specifies the rows attribute
               glyphicon: '', //optional, a glyphicon class (ex: glyphicon-minus), when type=button this can be set to add an icon to the button, when type=datetimepicker this can be set to override the default calendar icon
               onclick: function(){}, //optional, when type=button this specifies an onclick function
               htmlAttr: {}, //optional, when type=text||password||textarea this can be used to pass a set of html attributes, which will be set on the input element using jquery's attr method
               bindTo: 'fieldname' //this is only available when using comJSForm, specify the name or path of a field to bind to, this is not supported if type is 'html', 'button', or 'static'
             }
           ]
         },
         {
          grid: {
             id: 'grid', //an id for the grid
             add: true, //appears to not be in use
             title: 'grid title', //a title for the grid
             headers: [ //an array of objects with title values, for the grid column headings
               {title: 'Heading 1'},
               {title: 'Heading 2'}
             ],
             fields: [ //an array of fields within the current grid
               {
                //the other properties in here are the same as the ones as listed just above
               }
             ]
           }
         }
       ]
     }
   ]
 }
 */
function comJSForm(definition) {
  if (!definition) {
    throw new Error('You must supply a form definition');
  }
  this._isRendered = false;
  this._definition = definition;
  this._useBinding = definition['useBinding'] || false;
  this._model = null;
  this.comJSForm = new comJS_form({
    id: definition['id'] || 'new_form',
    title: definition['title'],
    rootPath: definition['rootPath'],
    success: definition['success'] || function () {
    }
  });
  var that = this;
  var bindableTypes = ['text', 'dropdown', 'textarea', 'checkbox', 'radio', 'password', 'multiselect', 'datetimepicker', 'daterangepicker', 'dropzone'];
  $.each(definition['sections'] || [], function (i, sectionInfo) {
    var section = that.comJSForm.addSection({
      id: sectionInfo['id'] || 'section' + i,
      title: sectionInfo['title'],
      className: sectionInfo['className']
    });
    $.each(sectionInfo['rows'] || [], function (y, row) {
      if (row['fields']) {
        row['fields'].forEach(function (field) {
          var type = field['type'] || 'text';
          if (field['bindTo'] && bindableTypes.indexOf(type) === -1) {
            throw new Error('Error in field ' + (field['id'] || 'no id') + ', fields of type ' + type + ' cannot use bindTo.');
          }
        });
        section.addRow(row['fields']);
      } else if (row['grid']) {
        section.addGrid(row['grid']);
      }
    });
  });
}

comJSForm.prototype.render = function (options) {
  //options can be a string OR an object:
  //string: a css selector string of an element to append the form to, ex: '#my_form_container'
  //object: {
  //target: '#element_id', //required. a css selector string of an element to append the form to, ex: '#my_form_container'
  //formValidationSettings: {} //optional, when specified, the attributes in here are passed through to the formValidation constructor: http://formvalidation.io/settings/
  //}
  if (this._isRendered) {
    throw new Error('This form is already rendered');
  }
  if (typeof options == 'string') {
    options = {
      target: options
    };
  }
  this.comJSForm.render(options);
  this._isRendered = true;
  if (this._useBinding) {
    if (this._model) {
      this._fillFromModel(this._model);
    }
    this._watchChanges();
  }
};

comJSForm.prototype.setModel = function (object) {
  if (object && typeof object['get'] !== 'function') {
    throw new Error('Model must be a ComJSModel object');
  }
  this._model = null;
  this._fillFromModel(object);
  this._model = object;
};

comJSForm.prototype._fillFromModel = function (model) {
  var this1 = this;
  if (this._isRendered) {
    (this._definition['sections'] || []).forEach(function (sectionInfo) {
      (sectionInfo['rows'] || []).forEach(function (row) {
        var isGrid = false;
        var models = [model];
        if (row['grid']) {
          isGrid = true;

          // Initial ComJSModel value
          var modelInit = comJSForm._getModelInitValue(row['grid']);

          var bindTo = row['grid']['bindTo'];
          if (bindTo) {
            if (!model.get(bindTo)) {
              model.set(bindTo, new ComJSCollection());
            }
            if (!model.get(bindTo).models) {
              var arr = model.get(bindTo);
              for (var i = 0, l = arr.length; i < l; i++) {
                arr[i] = $.extend({}, modelInit, arr[i]);
              }
              model.set(bindTo, new ComJSCollection(arr));
            }
            if (model.get(bindTo).models.length == 0) {
              model.get(bindTo).push(new ComJSModel(modelInit));
            }
            models = model.get(bindTo).models;
            $('#' + row['grid'].id + ' [data-row-index]').not('[data-row-index="0"]').each(function () {
              $(this).remove();
            });
            this1.comJSForm[row['grid'].id + '-index'] = 0;
          } else {
            models = [];
          }
          row = row['grid'];
        }

        (models).forEach(function (model, idx) {
          if (isGrid && idx > 0) {
            $('#' + row.id + ' button.grid-add').trigger('click');
          }
          var prefix = isGrid ? 'row[' + idx + '].' : '';
          (row['fields'] || []).forEach(function (field) {
            //TODO: support grids
            if (field['bindTo']) {
              var value = model ? (model.get(field['bindTo']) || '') : '';
              switch (field['type']) {
                case 'radio':
                case 'checkbox':
                  $.makeArray(value).forEach(function (val) {
                    var fld;
                    if (isGrid) {
                      fld = $('input[name="' + prefix + field['id'] + '"][value="' + val + '"]');
                    } else {
                      fld = $('input[name="' + field['id'] + '"][value="' + val + '"]');
                    }
                    if (fld.length) {
                      fld[0].checked = true;
                    }
                  });
                  break;
                case 'multiselect':
                  if (isGrid) {
                    $('[name="' + prefix + field['id'] + '"]').multiselect('select', $.makeArray(value));
                  } else {
                    $('#' + field['id']).multiselect('select', $.makeArray(value));
                  }
                  break;
                case 'datetimepicker':
                  if (isGrid) {
                    $('[name="' + prefix + field['id'] + '"]').parent().data("DateTimePicker").date(value);
                  } else {
                    $('.datetimepicker.' + field['id']).data("DateTimePicker").date(value);
                  }
                  break;
                case 'daterangepicker':
                  var picker;
                  if (isGrid) {
                    picker = $('[name="' + prefix + field['id'] + '"]').data('daterangepicker');
                  } else {
                    picker = $('#' + field['id']).data('daterangepicker');
                  }
                  if (value.indexOf(picker.locale.separator) > -1) {
                    var dates = value.split(picker.locale.separator);
                    picker.setStartDate(dates[0]);
                    picker.setEndDate(dates[1]);
                  }
                  break;
                case 'dropzone':
                  var dz;
                  if (isGrid) {
                    dz = $('[name="' + field['id'] + '"]').get(0);
                  } else {
                    dz = $('#' + field['id']).get(0);
                  }
                  if (dz && dz.comJSDropzone) {
                    dz.comJSDropzone._fillFromModel(model);
                  }
                  break;
                default:
                  if (isGrid) {
                    $('[name="' + prefix + field['id'] + '"]').val(value);
                  } else {
                    $('#' + field['id']).val(value);
                  }
                  break;
              }
            }
          });
        });
      });
    });
  }
};
comJSForm.prototype._watchChanges = function () {
  var form = this;
  if (this._isRendered) {
    (this._definition['sections'] || []).forEach(function (sectionInfo) {
      (sectionInfo['rows'] || []).forEach(function (row) {
        var isGrid = false;
        var prefixes = [''];
        if (row['grid']) {
          var gridDef = row['grid'];
          isGrid = true;
          prefixes = [];
          $('#' + gridDef.id + ' [data-row-index]').each(function (idx) {
            prefixes.push('row[' + idx + '].');
          });

          // Initial ComJSModel value
          var modelInit = comJSForm._getModelInitValue(gridDef);

          // Add button
          // $('#' + gridDef.id + ' button.grid-add').on('click', function(e) {
          var addButton = $('#' + gridDef.id + ' button.grid-add').get(0);
          addButton.onclick = (function (oldOnClick) {
            return function (e) {
              oldOnClick(e);

              var $newRow = $('#' + gridDef.id + ' [data-row-index]:last');

              // Remove button
              var $minButton = $('button.grid-minus', $newRow);
              $minButton.off('click').on('click', function (e) {
                var $gridRows = $('#' + gridDef.id + ' [data-row-index]');
                var $currentRow = $(this).closest('[data-row-index]');
                var idx = $gridRows.index($currentRow);

                if (form._model && form._model.get(gridDef['bindTo'])) {
                  var collection = form._model.get(row['bindTo']);
                  if (collection && collection.models) {
                    collection.remove(collection.models[idx]);
                  }
                }

                // ORIGINAL CODE
                var $row = $(this).closest('tr');
                $.each($row.find('.form-control'), function (i, item) {
                  var $item = $(item);
                  var itemId = item.tagName.toUpperCase() === 'FIELDSET' ? $item.find('input').first().attr('name') : $item.attr('name');
                  $('#' + form.id).formValidation('removeField', itemId);
                });
                var focusEl = $row.prev().find('input,select,textarea').first();
                $row.remove();
                focusEl.focus();
              });

              if (form._model && form._model.get(gridDef['bindTo'])) {
                var collection = form._model.get(row['bindTo']);
                if (collection) {
                  collection.push(new ComJSModel(modelInit));
                }
              }
              var prefixIdx = +$newRow.attr('data-row-index');
              var prefix = 'row[' + prefixIdx + '].';
              (gridDef['fields'] || []).forEach(function (field) {
                comJSForm._watchChanges_field(form, gridDef, field, true, prefix, prefixIdx); // TODO - Simplify arguments.
              });
            }
          })(addButton.onclick);
          // });

          row = row['grid'];
        }
        (prefixes).forEach(function (prefix, prefixIdx) {
          (row['fields'] || []).forEach(function (field) {
            comJSForm._watchChanges_field(form, row, field, isGrid, prefix, prefixIdx); // TODO - Simplify arguments.
          });
        });
      });
    });
  }
};
comJSForm._watchChanges_field = function (form, row, field, isGrid, prefix, prefixIdx) {
  function getModel(element) {
    var model = form._model;
    if (model && isGrid && form._model.get(row['bindTo']) && form._model.get(row['bindTo']).models) { // && form._model.get(row['bindTo']).models[prefixIdx]) {
      var $gridRows = $('#' + row.id + ' [data-row-index]');
      var $currentRow = $(element).closest('[data-row-index]');
      var idx = $gridRows.index($currentRow);
      if (form._model.get(row['bindTo']).models[idx]) {
        model = form._model.get(row['bindTo']).models[idx];
      }
    }
    return model;
  }

  //TODO: support grids
  if (field['bindTo']) {
    if (field['type'] === 'radio') {
      $('input[name="' + prefix + field['id'] + '"]').on('click', function (e) {
        var model = getModel(this);
        if (model) {
          model.set(field['bindTo'], $(e.currentTarget).val());
        }
      });
    } else if (field['type'] === 'checkbox') {
      $('input[name="' + prefix + field['id'] + '"]').on('click', function (e) {
        var model = getModel(this);
        if (model) {
          var value = $(e.currentTarget).val();
          var values = $.makeArray(model.get(field['bindTo']) || []).slice();
          var currentIndex = (values).indexOf(value);
          if (e.currentTarget.checked && currentIndex == -1) {
            values.push(value);
          } else if (!e.currentTarget.checked && currentIndex > -1) {
            values.splice(currentIndex, 1);
          }
          model.set(field['bindTo'], values);
        }
      });
    } else if (field['type'] === 'datetimepicker') {
      var $el;
      if (isGrid) {
        $el = $('[name="' + prefix + field['id'] + '"]').parent();
      } else {
        $el = $(".datetimepicker." + field['id']);
      }
      $el.on('dp.change', function () {
        var model = getModel(this);
        if (model) {
          model.set(field['bindTo'], $("#" + field['id']).val());
        }
      });
    } else if (field['type'] == 'dropzone') {
      var dz;
      if (isGrid) {
        dz = $('[name="' + field['id'] + '"]').get(0);
      } else {
        dz = $('#' + field['id']).get(0);
      }
      if (dz && dz.comJSDropzone) {
        dz.comJSDropzone._watchChanges(form);
      }
    } else {
      var $el;
      if (isGrid) {
        $el = $('[name="' + prefix + field['id'] + '"]');
      } else {
        $el = $('#' + field['id']);
      }
      $el.on('change', function (e) {
        var model = getModel(this);
        if (model) {
          var newVal = $(e.currentTarget).val();
          if (field['type'] === 'multiselect' && field['multiple'] && !newVal) {
            newVal = [];
          }
          model.set(field['bindTo'], newVal);
        }
      });
    }
  }
};

/*
 A convenience method to get all of the current form data as a javascript object,
 where each key is the name of the field and each value is the value

 */
comJSForm.prototype.getData = function () {
  var data = {};
  var $form = $('#' + this.comJSForm.id);
  $form.find('input[name], textarea[name], select[name]').each(function (i, fld) {
    var updateObject = data;
    var fieldName = fld.name;
    var fieldValue = null;
    if (fld.name.indexOf('row[') !== -1) {
      var sRowIndex = fld.name.substring(fld.name.indexOf('[') + 1, fld.name.indexOf(']'));
      if (sRowIndex !== 'template') {
        var gridId = $(fld).closest('.grid-object').attr('id');
        if (data[gridId] === undefined) {
          data[gridId] = [];
        }
        var iRowIndex = parseInt(sRowIndex);
        if (data[gridId][iRowIndex] === undefined) {
          data[gridId][iRowIndex] = {};
        }
        updateObject = data[gridId][iRowIndex];
        fieldName = fld.name.split('.')[1];
      } else {
        updateObject = null;
      }
    }
    if (updateObject) {
      switch (fld.tagName.toLowerCase()) {
        case 'input':
          switch (fld.type) {
            case 'text':
            case 'password':
              fieldValue = $(fld).val();
              break;
            case 'checkbox':
              fieldValue = fld.checked ? $(fld).val() : null;
              if (updateObject[fieldName] === undefined) {
                fieldValue = fieldValue === null ? [] : [fieldValue] //make sure checkbox values are always arrays
              } else if (fieldValue === null) {
                updateObject = null; //don't add nulls to the value of arrays
              }
              break;
            case 'radio':
              fieldValue = fld.checked ? $(fld).val() : null;
              if (updateObject[fieldName] === null) {
                delete updateObject[fieldName]; //overwrite null values with selected values
              } else if (updateObject[fieldName] !== undefined) {
                updateObject = null; //don't overwrite selected values with null values
              }
              break;
            default:
              updateObject = null;
              break;
          }
          break;
        case 'textarea':
          fieldValue = $(fld).val();
          break;
        case 'select':
          if (fld.multiple) {
            var v = $(fld).val();
            fieldValue = v === null ? [] : v; //always use arrays for select multiple=true
          } else {
            fieldValue = $(fld).val();
          }
          break;
        default:
          updateObject = null;
          break;
      }
    }
    if (updateObject) {
      if (updateObject.hasOwnProperty(fieldName)) {
        updateObject[fieldName] = $.makeArray(updateObject[fieldName]);
        if ($.isArray(fieldValue)) {
          updateObject[fieldName] = updateObject[fieldName].concat(fieldValue);
        } else {
          updateObject[fieldName].push(fieldValue);
        }
      } else {
        updateObject[fieldName] = fieldValue;
      }
    }
  });
  return data;
};

comJSForm._getModelInitValue = function (gridDef) {
  var modelInit = {};
  for (var i = 0, l = gridDef.fields.length; i < l; i++) {
    var field = gridDef.fields[i];
    if (field.bindTo) {
      if (field.type === 'checkbox' || (field.type === 'multiselect' && field.multiple === true)) {
        modelInit[field.bindTo] = [];
      }
      if (field.type === 'radio') {
        modelInit[field.bindTo] = null;
      } else {
        modelInit[field.bindTo] = '';
      }
    }
  }
  return modelInit;
};
