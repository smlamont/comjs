var waitForFinalEvent = (function () {
    var timers = {};
    return function (callback, ms, uniqueId) {
        if (!uniqueId) {
            uniqueId = "Don't call this twice without a uniqueId";
        }
        if (timers[uniqueId]) {
            clearTimeout (timers[uniqueId]);
        }
        timers[uniqueId] = setTimeout(callback, ms);
    };
})();

(function () {
    function resizeElements(parentElement, childElement){
        $(parentElement + " " + childElement).css("height","auto");
        $(parentElement).each( function(pindex, pvalue) {
            var maxHeight = 0;
            $(pvalue).find(childElement).each(function(cindex, cvalue){
                if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
            });
            $(pvalue).find(childElement).height(maxHeight);
        })
    }
    resizeElements(".featured", ".feature-card");
    resizeElements(".menu", ".menu-card");
    $( window ).resize(function() {
        waitForFinalEvent(function(){
            resizeElements(".featured", ".feature-card");
            resizeElements(".menu", ".menu-card");

            $('img.svg').each(function(){
                var $img = $(this);
                var imgID = $img.attr('id');
                var imgClass = $img.attr('class');
                var imgURL = $img.attr('src');

                $.get(imgURL, function(data) {
                    // Get the SVG tag, ignore the rest
                    var $svg = $(data).find('svg');

                    // Add replaced image's ID to the new SVG
                    if(typeof imgID !== 'undefined') {
                        $svg = $svg.attr('id', imgID);
                    }
                    // Add replaced image's classes to the new SVG
                    if(typeof imgClass !== 'undefined') {
                        $svg = $svg.attr('class', imgClass+' replaced-svg');
                    }

                    // Remove any invalid XML tags as per http://validator.w3.org
                    $svg = $svg.removeAttr('xmlns:a');

                    // Replace image with new SVG
                    $img.replaceWith($svg);

                }, 'xml');

            });

        }, 100, "resizecards");
    });
}());





