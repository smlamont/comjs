// The main javascript file for my_app_name.
// IMPORTANT:
// Any resources from this project should be referenced using SRC_PATH preprocessor var
// Ex: let myImage = '/*@echo SRC_PATH*//img/sample.jpg';

$(function () {
  if (window['comJS_app']) { //the code in this 'if' block should be deleted for embedded apps
    const app = new comJS_app("my_app_name",{
      hasContentTop: false,
      hasContentBottom: false,
      hasContentRight: false,
      hasContentLeft: false,
      searchcontext: 'INTRA'
    });

    app.setBreadcrumb([
      {"name": "my_app_name", "link": "#"}
    ]).render();
  }
  let container = $('#my_app_name_container');
});
