my_app_name
===========
Describe your app.

For embedded apps:
------------------
This app can be embedded on City Wordpress pages with:

`[comJS_app app="my_app_name"][/comJS_app]`

Here is the application.json definition

`{"id": "my_app_name", "title": "My App Name", "description": "Describe your app."}`
