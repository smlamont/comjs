const gulp = require('gulp');
const comJS = require('./node_modules/@comJS_components/comJS/gulp_helper');
const pkg = require('./package.json');

comJS.createTasks(gulp, {
  pkg,
  embedArea: 'full',
  environmentOverride: null,
  deploymentPath: '',
  preprocessorContext: {
    local: {},
    dev: {},
    qa: {},
    prod: {}
  }
});
